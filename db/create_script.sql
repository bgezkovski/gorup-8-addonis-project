create table addon_images
(
    image_id int auto_increment
        primary key,
    name     varchar(2000) not null,
    type     varchar(50)   null,
    image    mediumblob    null
);

create table addons_binary
(
    addon_id     int      not null
        primary key,
    addon_binary longblob not null
);

create table images
(
    image_id int auto_increment
        primary key,
    name     varchar(2000) not null,
    type     varchar(50)   null,
    image    mediumblob    null
);

create table roles
(
    role_id   int auto_increment
        primary key,
    role_name varchar(20) not null
);

create table tags
(
    tag_id   int auto_increment
        primary key,
    tag_name varchar(20) not null
);

create table users
(
    user_id      int auto_increment
        primary key,
    username     varchar(32)  not null,
    password     varchar(264) not null,
    email        varchar(50)  not null,
    phone        varchar(10)  not null,
    role_id      int          not null,
    first_name   varchar(20)  not null,
    last_name    varchar(20)  not null,
    is_activated tinyint      not null comment 'isActivated',
    is_blocked   tinyint(1)   not null,
    image_id     int          null,
    constraint users_images_fk
        foreign key (image_id) references images (image_id),
    constraint users_roles_fk
        foreign key (role_id) references roles (role_id)
);

create table addons
(
    addon_id            int auto_increment
        primary key,
    name                varchar(30)   not null,
    target_IDE          varchar(30)   not null,
    description         varchar(5000) not null,
    creation_time       datetime      not null,
    downloads           int           not null,
    binary_content      varchar(200)  null,
    link_Git            varchar(200)  not null,
    user_id             int           not null,
    open_issues         int           not null,
    pull_requests_count int           not null,
    last_commit_message varchar(512)  null,
    last_commit_date    date          null,
    image_id            int           null,
    is_featured         tinyint(1)    not null,
    is_approved         tinyint(1)    not null,
    constraint addons_addon_images_fk
        foreign key (image_id) references addon_images (image_id),
    constraint addons_users_fk
        foreign key (user_id) references users (user_id)
);

create table addons_ratings
(
    addon_rating_id int auto_increment
        primary key,
    addon_id        int    not null,
    user_id         int    not null,
    rating          int(1) not null,
    constraint addons_ratings_addons_fk
        foreign key (addon_id) references addons (addon_id),
    constraint addons_ratings_users_fk
        foreign key (user_id) references users (user_id)
);

create table addons_tags
(
    addon_id int not null,
    tag_id   int not null,
    constraint addons_tags_addons_fk
        foreign key (addon_id) references addons (addon_id),
    constraint addons_tags_tags_fk
        foreign key (tag_id) references tags (tag_id)
);
