# Addonis

<img src="images/addonis_main_page.png">

Addonis is a plugin repository developed by <br/> 
Boris Gezkovski, Borislav Borisov and Zhasmina Zheleva as our final project for Telerik Academy.<br/>
Along side the [website](http://54.93.184.1) we also have a fully functioning API you can look at the docs [here](http://54.93.184.1/swagger-ui/)  
 

## Description 
The tech stack we use is:<br/>
[Spring Framework](https://spring.io/)<br/>
[MariaDB](https://mariadb.org/)<br/>
[Bootstap](https://getbootstrap.com/)<br/>
[Docker](https://www.docker.com/)<br/>

### Database layout
<img src="images/db_schema.png">

## Project setup
To set project up localy use the provided database setup scripts and make the needed changes in application.properties file </br>
Then run the project and head to localhost:8080 to register a new account or go to localhost:8080/swagger-ui for the docs



