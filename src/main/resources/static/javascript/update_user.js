const register_form = document.getElementById('registerForm')
// User Id
const url = window.location.pathname;
const user_id = url.substring(url.lastIndexOf('/') + 1);

// Input fields
const first_name_field = document.getElementById('firstName')
const last_name_field = document.getElementById('lastName')
const email_field = document.getElementById('emailField')
const username_field = document.getElementById('usernameField')
const phone_number_field = document.getElementById('phoneField')
const password_field = document.getElementById('passwordField')
const password_verification_field = document.getElementById('passwordVerificationField')

// Error fields
const first_name_error_field = document.getElementById('first-name-error')
const last_name_error_field = document.getElementById('last-name-error')
const username_error_field = document.getElementById('username-error')
const email_error_field = document.getElementById('email-error')
const phone_number_error_field = document.getElementById('phone-error')
const password_error_field = document.getElementById('password-error')
const password_verification_error_field = document.getElementById('password-verification-error')

let registered_phones = []
let registered_emails = []
let registered_usernames = []

    fetch("http://54.93.184.1/api/users/register_data", {
        method: "GET"
    }).then(res => res.json())
        .then(data => {
            registered_emails = data['emails']
            registered_phones = data['phoneNumbers']
            registered_usernames = data['usernames']
        })
        .catch(err => console.log(err))



register_form.addEventListener('submit', e => {
    reset_errors()
    if(!register_form.checkValidity()) {
        register_form.classList.add('was-validated')
        e.preventDefault()
        return
    }

    if(!validate_input_fields()){
        e.preventDefault()
    }else{
        submit_form()
        e.preventDefault();
    }
})

function validate_input_fields(){
    let passed_validation = true
    const mail_regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    const password_regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/

    if(first_name_field.value.length < 2 || first_name_field.value.length > 20){
        first_name_error_field.textContent = 'First name should be between 2 and 20 symbols'
        passed_validation = false
    }

    if(last_name_field.value.length < 2 || last_name_field.value.length > 20){
        last_name_error_field.textContent = 'Last name should be between 2 and 20 symbols'
        passed_validation = false
    }

    if(!email_field.value.match(mail_regex)){
        email_error_field.textContent = 'Please provide a valid mail format'
        passed_validation = false
    }

    if(registered_emails.includes(email_field.value)){
        email_error_field.textContent = 'User with email ' + email_field.value + ' already exists'
        passed_validation = false
    }

    if(username_field.value.length < 2 || username_field.value.length > 20){
        username_error_field.textContent = 'Username must be between 2 and 20 characters long'
        passed_validation = false
    }

    if(registered_usernames.includes(username_field.value)){
        username_error_field.textContent = 'Username taken'
        passed_validation = false
    }

    if(phone_number_field.value.length !== 10){
        phone_number_error_field.textContent = 'Phone number must be 10 digits long'
        passed_validation = false
    }

    if(registered_phones.includes(phone_number_field.value)){
        phone_number_error_field.textContent = 'User with phone number ' + phone_number_field.value + ' already exists'
        passed_validation = false
    }

    if(!password_field.value.match(password_regex)){
        password_error_field.textContent = 'Password must be at least 8 characters long and contain one uppercase, one lowercase letter a digit and a special symbol'
        passed_validation = false
    }

    if(password_field.value !== password_verification_field.value){
        password_verification_error_field.textContent = 'Passwords must match'
        passed_validation = false
    }
    return passed_validation
}

function reset_errors(){
    first_name_error_field.textContent = ''
    last_name_error_field.textContent = ''
    email_error_field.textContent = ''
    username_error_field.textContent = ''
    phone_number_error_field.textContent = ''
    password_error_field.textContent = ''
    password_verification_error_field.textContent = ''
}

function submit_form(){
    show_loading()
    fetch("http://localhost:8080/api/users", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
                        "firstName": first_name_field.value,
                        "lastName": last_name_field.value,
                        "email": email_field.value,
                        'username': username_field.value,
                        'password': password_field.value,
                        'phone': phone_number_field.value
        })
    })
        .then(res => {
            res.json()
        })
        .then(data => {
            post_register_login()
            hide_loading()
        })
        .catch(err => console.log(err))
}

function show_loading(){
    const button = document.getElementById("register-button")
    button.disabled = true
    button.innerHTML = '<span class="spin"> <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-arrow-clockwise" viewBox="0 0 16 16">\n' +
        '  <path fill-rule="evenodd" d="M8 3a5 5 0 1 0 4.546 2.914.5.5 0 0 1 .908-.417A6 6 0 1 1 8 2v1z"/>\n' +
        '  <path d="M8 4.466V.534a.25.25 0 0 1 .41-.192l2.36 1.966c.12.1.12.284 0 .384L8.41 4.658A.25.25 0 0 1 8 4.466z"/>\n' +
        '</svg></span>'
}

function hide_loading(){
    const button = document.getElementById("register-button")
    button.disabled = false
    button.innerHTML = 'Sign up'
}

function post_register_login(){
    const login_username_field = document.getElementById('username-input')
    const login_password_field = document.getElementById('password-input')
    const login_form = document.getElementById('loginForm')
    login_username_field.value = username_field.value
    login_password_field.value = password_field.value
    login_form.submit()
}
