package com.example.addonis.models;

import org.hibernate.validator.constraints.Range;

import javax.persistence.*;

@Entity
@Table(name = "addons_ratings")
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "addon_rating_id", nullable = false, unique = true, updatable = false)
    private int id;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "addon_id", nullable = false)
    private Addon addon;

    @Column(name = "rating", nullable = false)
    @Range(min = 1, max = 5, message = "Rating should be between 1 and 5")
    private int rating;

    public Rating() {
    }

    public Rating(int id, User user, Addon addon, int rating) {
        this.id = id;
        this.user = user;
        this.addon = addon;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Addon getAddon() {
        return addon;
    }

    public void setAddon(Addon addon) {
        this.addon = addon;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}