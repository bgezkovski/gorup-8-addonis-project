package com.example.addonis.models;

import javax.persistence.*;

@Entity
@Table(name = "addon_images")
public class AddonImage {

    @Id
    @Column(name = "image_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int imageId;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private String type;

    @Column(name = "image", nullable = false, length = 100000)
    private byte[] image;


    public AddonImage() {
    }

    public AddonImage(String name, String type, byte[] image) {
        this.name = name;
        this.type = type;
        this.image = image;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int id) {
        this.imageId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

}

