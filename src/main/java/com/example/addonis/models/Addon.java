package com.example.addonis.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.Set;
@Entity
@Table(name = "addons")
public class Addon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "addon_id")
    private int addonId;
    @Column(name = "name")
    private String name;
    @Column(name = "target_IDE")
    private String targetIDE;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User creator;
    @Column(name = "description")
    private String description;
    @Column(name = "creation_time")
    @CreationTimestamp
    private LocalDateTime creationTime;

    @Column(name = "downloads")
    private int downloads;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "addons_tags",
            joinColumns = @JoinColumn(name = "addon_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private Set<Tag> tags;


    @Column(name = "open_issues")
    private int openIssues;

    @Column(name = "pull_requests_count")
    private int pullRequests;

    @Column(name = "last_commit_date")
    private Date lastCommitDate;

    @Column(name = "last_commit_message")
    private String lastCommitMessage;

    @Column(name = "link_Git")
    private String sourceUrl;

    @Column(name = "is_featured")
    private boolean isFeatured;

    @Column(name = "is_approved")
    private boolean isApproved;

    @ManyToOne
    @JoinColumn(name = "image_id")
    @NotNull(message = "Image id can't be empty")
    private AddonImage image;

    @Transient
    private long averageRating;


    public Addon() {
    }

    public Addon(String name,
                 String targetIDE,
                 User creator,
                 String description,
                 LocalDateTime creationTime,
                 int downloads,
                 String sourceUrl,
                 AddonImage image,
                 boolean isFeatured) {
        this.name = name;
        this.targetIDE = targetIDE;
        this.creator = creator;
        this.description = description;
        this.creationTime = creationTime;
        this.downloads = downloads;
        this.sourceUrl = sourceUrl;
        this.image = image;
        this.isFeatured = isFeatured;
    }

    public Addon(int addonId, String name, String targetIDE, User creator,
                 String description, Set<Tag> tags) {
        this.addonId = addonId;
        this.name = name;
        this.targetIDE = targetIDE;
        this.creator = creator;
        this.description = description;
        this.tags = tags;

    }

    public int getAddonId() {
        return addonId;
    }

    public void setAddonId(int addonId) {
        this.addonId = addonId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTargetIDE() {
        return targetIDE;
    }

    public void setTargetIDE(String targetIDE) {
        this.targetIDE = targetIDE;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public int getDownloads() {
        return downloads;
    }

    public void setDownloads(int downloads) {
        this.downloads = downloads;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public int getOpenIssues() {
        return openIssues;
    }

    public void setOpenIssues(int openIssues) {
        this.openIssues = openIssues;
    }

    public int getPullRequests() {
        return pullRequests;
    }

    public void setPullRequests(int pullRequests) {
        this.pullRequests = pullRequests;
    }

    public Date getLastCommitDate() {
        return lastCommitDate;
    }

    public void setLastCommitDate(Date lastCommitDate) {
        this.lastCommitDate = lastCommitDate;
    }

    public String getLastCommitMessage() {
        return lastCommitMessage;
    }

    public void setLastCommitMessage(String lastCommitMessage) {
        this.lastCommitMessage = lastCommitMessage;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public AddonImage getImage() {
        return image;
    }

    public void setImage(AddonImage image) {
        this.image = image;
    }

    public long getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(long averageRating) {
        this.averageRating = averageRating;
    }


    public boolean isFeatured() {
        return isFeatured;
    }

    public void setFeatured(boolean featured) {
        isFeatured = featured;
    }

    public boolean isApproved() {
        return isApproved;
    }

    public void setApproved(boolean approved) {
        isApproved = approved;
    }

}