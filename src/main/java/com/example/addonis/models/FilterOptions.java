package com.example.addonis.models;

import java.util.Optional;

public class FilterOptions {

    private Optional<String> name;
    private Optional<String> targetIde;
    private Optional<String> sortBy;
    private Optional<String> sortOrder;

    public FilterOptions() {
        this(null, null, null, null);
    }

    public FilterOptions(String name, String targetIDE, String sortBy, String sortOrder) {
        this.name = Optional.ofNullable(name);
        this.targetIde = Optional.ofNullable(targetIDE);
        this.sortBy = Optional.ofNullable(sortBy);
        this.sortOrder = Optional.ofNullable(sortOrder);
    }

    public Optional<String> getName() {
        return name;
    }

    public void setName(Optional<String> title) {
        this.name = title;
    }

    public Optional<String> getTargetIde() {
        return targetIde;
    }

    public void setTargetIde(Optional<String> content) {
        this.targetIde = content;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public void setSortBy(Optional<String> sortBy) {
        this.sortBy = sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Optional<String> sortOrder) {
        this.sortOrder = sortOrder;
    }
}
