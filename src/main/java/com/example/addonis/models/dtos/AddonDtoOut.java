package com.example.addonis.models.dtos;

import com.example.addonis.models.Tag;

import java.time.LocalDateTime;
import java.util.Set;

public class AddonDtoOut {

    private String name;
    private String targetIDE;
    private String description;
    private LocalDateTime creationTime;
    private int downloads;
    private UserDtoOut createdBy;
    private Set<Tag> tags;
    private String binaryContent;

    public AddonDtoOut() {
    }



    public AddonDtoOut(String name, String targetIDE, String description,
                       UserDtoOut createdBy, String binaryContent) {
        this.name = name;
        this.targetIDE = targetIDE;
        this.description = description;
        this.createdBy = createdBy;
        this.binaryContent = binaryContent;
    }

    public AddonDtoOut(String name, String targetIDE, String description, LocalDateTime creationTime,
                       int downloads, UserDtoOut createdBy, Set<Tag> tags) {
        this.name = name;
        this.targetIDE = targetIDE;
        this.description = description;
        this.creationTime = creationTime;
        this.downloads = downloads;
        this.createdBy = createdBy;
        this.tags = tags;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTargetIDE() {
        return targetIDE;
    }

    public void setTargetIDE(String targetIDE) {
        this.targetIDE = targetIDE;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public int getDownloads() {
        return downloads;
    }

    public void setDownloads(int downloads) {
        this.downloads = downloads;
    }

    public UserDtoOut getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(UserDtoOut createdBy) {
        this.createdBy = createdBy;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public String getBinaryContent() {
        return binaryContent;
    }

    public void setBinaryContent(String binaryContent) {
        this.binaryContent = binaryContent;
    }
}