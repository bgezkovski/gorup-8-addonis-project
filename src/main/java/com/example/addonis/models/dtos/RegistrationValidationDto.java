package com.example.addonis.models.dtos;

import java.util.List;

public class RegistrationValidationDto {

    private List<String> emails;
    private List<String> usernames;
    private List<String> phoneNumbers;

    public RegistrationValidationDto(List<String> emails, List<String> usernames, List<String> phoneNumbers) {
        this.emails = emails;
        this.usernames = usernames;
        this.phoneNumbers = phoneNumbers;
    }

    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    public List<String> getUsernames() {
        return usernames;
    }

    public void setUsernames(List<String> usernames) {
        this.usernames = usernames;
    }

    public List<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

}
