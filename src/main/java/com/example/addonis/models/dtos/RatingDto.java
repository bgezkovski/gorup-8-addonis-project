package com.example.addonis.models.dtos;

public class RatingDto {

    int vote;

    public RatingDto() {
    }

    public RatingDto(int vote) {
        this.vote = vote;
    }

    public int getVote() {
        return vote;
    }

    public void setVote(int vote) {
        this.vote = vote;
    }


}
