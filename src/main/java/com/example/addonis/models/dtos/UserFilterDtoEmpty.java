package com.example.addonis.models.dtos;

import java.util.Optional;

public class UserFilterDtoEmpty {
    Optional<String> username;
    Optional<String> roleName;
    Optional<String> email;
    Optional<String> phone;
    Optional<String> sortBy;
    Optional<String> sortOrder;

    public UserFilterDtoEmpty(String username,
                              String roleName,
                              String email,
                              String phone,
                              String sortBy,
                              String sortOrder) {
        this.username = Optional.ofNullable(username);
        this.roleName = Optional.ofNullable(roleName);
        this.email = Optional.ofNullable(email);
        this.phone = Optional.ofNullable(phone);
        this.sortBy = Optional.ofNullable(sortBy);
        this.sortOrder = Optional.ofNullable(sortOrder);
    }

    public UserFilterDtoEmpty() {

    }

    public Optional<String> getUsername() {
        return username;
    }

    public void setUsername(Optional<String> username) {
        this.username = username;
    }

    public Optional<String> getRoleName() {
        return roleName;
    }

    public void setRoleName(Optional<String> roleName) {
        this.roleName = roleName;
    }

    public Optional<String> getEmail() {
        return email;
    }

    public void setEmail(Optional<String> email) {
        this.email = email;
    }

    public Optional<String> getPhone() {
        return phone;
    }

    public void setPhone(Optional<String> phone) {
        this.phone = phone;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public void setSortBy(Optional<String> sortBy) {
        this.sortBy = sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Optional<String> sortOrder) {
        this.sortOrder = sortOrder;
    }
}
