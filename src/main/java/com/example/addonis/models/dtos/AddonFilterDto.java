package com.example.addonis.models.dtos;

import java.util.Optional;

public class AddonFilterDto {

    Optional<String> name;
    Optional<String> targetIde;
    Optional<String> sortParam;


    public AddonFilterDto(Optional<String> name, Optional<String> targetIde, Optional<String> sortParam) {
        this.name = name;
        this.targetIde = targetIde;
        this.sortParam = sortParam;

    }

    public AddonFilterDto() {
    }

    public Optional<String> getName() {
        return name;
    }

    public void setName(Optional<String> name) {
        this.name = name;
    }


    public Optional<String> getTargetIde() {
        return targetIde;
    }

    public void setTargetIde(Optional<String> targetIde) {
        this.targetIde = targetIde;
    }

    public Optional<String> getSortParam() {
        return sortParam;
    }

    public void setSortParam(Optional<String> sortParam) {
        this.sortParam = sortParam;
    }
}

