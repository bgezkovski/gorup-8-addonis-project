package com.example.addonis.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class TagDto {

    @NotNull(message = "Tag name can't be empty")
    @Size(min = 4, max = 15, message = "Tag name should be between 4 and 15 symbols")
    private String name;

    public TagDto() {
    }

    public TagDto(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
