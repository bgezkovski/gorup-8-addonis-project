package com.example.addonis.models.dtos;

import com.example.addonis.models.AddonImage;
import com.example.addonis.models.Tag;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Set;

public class AddonDto {

    @NotNull(message = "Addon's name can't be empty")
    @Size(min = 3, max = 30, message = "Addon's name should be between 3 and 30 symbols")
    private String name;

    private String targetIDE;

    @NotNull(message = "Tag name can't be empty")
    @Size(min = 4, max = 5000, message = "Description should be between 4 and 5000 symbols")
    private String description;

    private LocalDateTime creationTime;

    private int downloads;

    private Set<Tag> tags;

    @Size(min = 4, max = 15, message = "Tag name should be between 4 and 15 symbols")
    private String tag;

    private String source_url;

    private String binaryContent;

    private AddonImage image;

    private MultipartFile file;

    public AddonDto() {
    }

    public AddonDto(String name,
                    String targetIDE,
                    String description,
                    LocalDateTime creationTime,
                    int downloads,
                    Set<Tag> tags,
                    String source_url) {

        this.name = name;
        this.targetIDE = targetIDE;
        this.description = description;
        this.creationTime = creationTime;
        this.downloads = downloads;
        this.tags = tags;
        this.source_url = source_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSource_url() {
        return source_url;
    }

    public void setSource_url(String source_url) {
        this.source_url = source_url;
    }

    public String getTargetIDE() {
        return targetIDE;
    }

    public void setTargetIDE(String targetIDE) {
        this.targetIDE = targetIDE;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public int getDownloads() {
        return downloads;
    }

    public void setDownloads(int downloads) {
        this.downloads = downloads;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public String getBinaryContent() {
        return binaryContent;
    }

    public void setBinaryContent(String binaryContent) {
        this.binaryContent = binaryContent;
    }

    public AddonImage getImage() {
        return image;
    }

    public void setImage(AddonImage image) {
        this.image = image;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}