package com.example.addonis.models.dtos;

public class CodeDto {

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    String code;

}
