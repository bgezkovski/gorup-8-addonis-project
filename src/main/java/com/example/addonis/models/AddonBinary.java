package com.example.addonis.models;


import javax.persistence.*;

@Entity
@Table(name = "addons_binary")
public class AddonBinary {

    @Id
    @Column(name = "addon_id")
    private int id;

    @Lob
    @Column(name = "addon_binary", columnDefinition = "LONGBLOB")
    private byte[] content;

    public AddonBinary() {}

    public AddonBinary(int id, byte[] content) {
        this.id = id;
        this.content = content;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
