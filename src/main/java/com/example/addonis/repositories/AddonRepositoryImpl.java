package com.example.addonis.repositories;

import com.example.addonis.exceptions.EntityNotFoundException;
import com.example.addonis.models.Addon;
import com.example.addonis.models.FilterOptions;
import com.example.addonis.models.dtos.AddonFilterDto;
import com.example.addonis.repositories.contracts.AddonRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class AddonRepositoryImpl implements AddonRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public AddonRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Addon getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Addon addon = session.get(Addon.class, id);
            if (addon == null) {
                throw new EntityNotFoundException("Addon", id);
            }
            return addon;
        }
    }

    @Override
    public Addon getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery("from Addon where name = :name", Addon.class);
            query.setParameter("name", name);
            List<Addon> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Addon", "name", name);
            }
            return result.get(0);
        }
    }

    @Override
    public List<Addon> getAddons() {
        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery("from Addon", Addon.class);
            return query.list();
        }
    }

    @Override
    public int create(Addon addon) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            int id = (Integer) session.save(addon);
            session.getTransaction().commit();
            return id;
        }
    }

    @Override
    public void update(Addon addon) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(addon);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Addon addonToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(addonToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Addon> filter(AddonFilterDto addonFilterDto) {
        FilterOptions filterOptions = new FilterOptions();
        filterOptions.setName(addonFilterDto.getName());
        filterOptions.setTargetIde(addonFilterDto.getTargetIde());

        if (addonFilterDto.getSortParam().isPresent()) {
            String[] params = addonFilterDto.getSortParam().get().split(",");
            if (params.length > 1) {
                filterOptions.setSortBy(Optional.of(params[0].trim().toLowerCase(Locale.ROOT)));
                filterOptions.setSortOrder(Optional.of(params[1].trim().toLowerCase(Locale.ROOT)));
            }
        }
        return get(filterOptions);
    }

    @Override
    public List<Addon> getAddons(Optional<String> search) {
        if (search.isEmpty()) {
            return getAddons();
        }

        try (Session session = sessionFactory.openSession()) {
            Query<Addon> query = session.createQuery(
                    "from Addon where name like :name " +
                            "or addonId = :addonId");
            query.setParameter("name", "%" + search.get() + "%");
            query.setParameter("addonId", getNumberIfPresent(search));
            return query.list();
        }
    }

    @Override
    public List<Addon> get(FilterOptions filterOptions) {
        try (Session session = sessionFactory.openSession()) {
            List<String> filters = new ArrayList<>();
            Map<String, Object> params = new HashMap<>();

            filterOptions.getName().ifPresent(value -> {
                filters.add("name like :name");
                params.put("name", "%" + value + "%");
            });

            filterOptions.getTargetIde().ifPresent(value -> {
                filters.add("targetIDE like :targetIDE");
                params.put("targetIDE", "%" + value + "%");
            });

            StringBuilder queryString = new StringBuilder("from Addon");
            if (!filters.isEmpty()) {
                queryString
                        .append(" where ")
                        .append(String.join(" and ", filters));
            }
            queryString.append(generateOrderBy(filterOptions));
            String check = queryString.toString();
            Query<Addon> query = session.createQuery(queryString.toString(), Addon.class);
            query.setProperties(params);
            return query.list();
        }
    }

    @Override
    public List<Addon> getMostRecent() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Addon order by creationTime desc", Addon.class)
                    .setMaxResults(10).list();
        }
    }

    @Override
    public List<Addon> getMostDownloaded() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Addon order by downloads desc", Addon.class)
                    .setMaxResults(10).list();
        }
    }

    @Override
    public List<Addon> getFeatured() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Addon where isFeatured = true", Addon.class)
                    .setMaxResults(10).list();
        }
    }

    @Override
    public List<Addon> getAddonsByUserId(int user_id) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createNativeQuery("select * from addons where user_id =:user_id");
            query.setParameter("user_id", user_id);
            return query.list();
        }
    }

    private String generateOrderBy(FilterOptions filterOptions) {
        if (filterOptions.getSortBy().isEmpty()) {
            return "";
        }

        String orderBy = "";
        switch (filterOptions.getSortBy().get()) {
            case "name":
                orderBy = "name";
                break;
            case "downloads":
                orderBy = "downloads";
            case "upload date":
                orderBy = "creationTime";
            case "last commit date":
                orderBy = "lastCommitDate";
                break;

        }
        orderBy = String.format(" order by %s", orderBy);
        if (filterOptions.getSortOrder().isPresent() && filterOptions.getSortOrder().get().equalsIgnoreCase("desc")) {
            orderBy = String.format("%s desc", orderBy);
        }
        return orderBy;
    }

    private int getNumberIfPresent(Optional<String> search) {
        try {
            return Integer.parseInt(search.get());
        } catch (NumberFormatException e) {
            return -1;
        }
    }

}