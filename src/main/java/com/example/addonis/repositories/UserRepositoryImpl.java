package com.example.addonis.repositories;

import com.example.addonis.exceptions.EntityNotFoundException;
import com.example.addonis.models.Role;
import com.example.addonis.models.User;
import com.example.addonis.models.dtos.UserFilterDto;
import com.example.addonis.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final SessionFactory sessionFactory;
    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
    private List<User> getUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User", User.class);
            return query.list();
        }
    }

    @Override
    public List<User> getUsers(Optional<String> search) {
        if (search.isEmpty()) {
            return getUsers();
        }

        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User where firstName like :firstName " +
                            "or lastName like :lastName or email like :email" +
                            " or username like :username or role.name like :roleName" +
                            " or phone like :phone" +
                            " or id = :id");
            query.setParameter("firstName", "%" + search.get() + "%");
            query.setParameter("lastName", "%" + search.get() + "%");
            query.setParameter("email", "%" + search.get() + "%");
            query.setParameter("username", "%" + search.get() + "%");
            query.setParameter("roleName", "%" + search.get() + "%");
            query.setParameter("phone", "%" + search.get() + "%");
            query.setParameter("id", getNumberIfPresent(search));
            return query.list();
        }
    }

    @Override
    public List<User> get(UserFilterDto userFilterDto) {
        try (Session session = sessionFactory.openSession()) {
            List<String> filters = new ArrayList<>();
            Map<String, Object> params = new HashMap<>();

            userFilterDto.getUsername().ifPresent(value -> {
                filters.add("username like :username");
                params.put("username", "%" + value + "%");
            });

            userFilterDto.getEmail().ifPresent(value -> {
                filters.add("email like :email");
                params.put("email", "%" + value + "%");
            });

            userFilterDto.getPhone().ifPresent(value -> {
                filters.add("phone like :phone");
                params.put("phone", "%" + value + "%");
            });

            userFilterDto.getRoleName().ifPresent(value -> {
                filters.add("role.name like :name");
                params.put("name", "%" + value + "%");
            });

            StringBuilder queryString = new StringBuilder("from User");
            if (!filters.isEmpty()) {
                queryString
                        .append(" where ")
                        .append(String.join(" and ", filters));
            }

            userFilterDto.getSortBy().ifPresent(value -> queryString.append(generateStringFromSort(value)));
            if(userFilterDto.getSortBy().isPresent()) {
                userFilterDto.getSortOrder().ifPresent(value -> queryString.append(value));
            }

            Query<User> query = session.createQuery(queryString.toString(), User.class);
            query.setProperties(params);
            return query.list();
        }
    }

    private String generateStringFromSort(String value) {
        var queryString = new StringBuilder(" order by ");

        if (value.isEmpty()){
            return "";
        }

        switch (value){
            case "username":
                queryString.append(" username ");
                break;
            case "roleName":
                queryString.append(" role.name ");
                break;
            case "email":
                queryString.append(" email ");
                break;
            case "lastName":
                queryString.append(" lastName ");
            case "phone":
                queryString.append(" phone ");
                break;
        }

        return queryString.toString();
    }

    @Override
    public User getById(int id) {
        try(Session session = sessionFactory.openSession()){
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    @Override
        public User getByUsername(String username) {
        try(Session session = sessionFactory.openSession()){
            Query query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);
            List<User> result = query.list();
            if (result.size() == 0){
                throw new EntityNotFoundException("User", "username", username);
            }
            return result.get(0);
        }
    }

    @Override
    public User getByEmail(String email) {
        try(Session session = sessionFactory.openSession()){
            Query query = session.createQuery("from User where email = :email", User.class);
            query.setParameter("email", email);
            List<User> result = query.list();
            if (result.size() == 0){
                throw new EntityNotFoundException("User", "email", email);
            }
            return result.get(0);
        }
    }

    @Override
    public User getByPhone(String phone) {
        try(Session session = sessionFactory.openSession()){
            Query query = session.createQuery("from User where phone = :phone", User.class);
            query.setParameter("phone", phone);
            List<User> result = query.list();
            if (result.size() == 0){
                throw new EntityNotFoundException("User", "email", phone);
            }
            return result.get(0);
        }
    }

    @Override
    public int getCount() {
        try(Session session = sessionFactory.openSession()){
            Long count = ((Long) session.createQuery("select count(*) from User ").uniqueResult());
            return count.intValue();
        }
    }

    @Override
    public void create(User user){
        try(Session session = sessionFactory.openSession()){
            session.save(user);
        }
    }

    @Override
    public void update(User user){
        try(Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id){
        User userToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(userToDelete);
            session.getTransaction().commit();
        }
    }

    private int getNumberIfPresent(Optional<String> search) {
        try {
            return Integer.parseInt(search.get());
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    @Override
    public Role getRoleById(int id) {
        try(Session session = sessionFactory.openSession()){
            Role role = session.get(Role.class, id);
            if (role == null) {
                throw new EntityNotFoundException("Role", id);
            }
            return role;
        }
    }

    @Override
    public List<Role> getRoles() {
        try (Session session = sessionFactory.openSession()) {
            Query<Role> query = session.createQuery("from Role", Role.class);
            return query.list();
        }
    }

    @Override
    public List<String> getAllEmails() {
        try(Session session = sessionFactory.openSession()){
            NativeQuery query = session.createNativeQuery("select email from users");
            return query.list();
        }
    }

    @Override
    public List<String> getAllPhoneNumbers() {
        try(Session session = sessionFactory.openSession()){
            NativeQuery query = session.createNativeQuery("select phone from users");
            return query.list();
        }
    }

    @Override
    public List<String> getAllUsernames() {
        try(Session session = sessionFactory.openSession()){
            NativeQuery query = session.createNativeQuery("select username from users");
            return query.list();
        }
    }

}