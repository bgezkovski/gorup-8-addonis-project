package com.example.addonis.repositories;

import com.example.addonis.exceptions.EntityNotFoundException;
import com.example.addonis.models.AddonImage;
import com.example.addonis.repositories.contracts.AddonImageRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AddonImageRepositoryImpl implements AddonImageRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public AddonImageRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int create(AddonImage image) {
        try (Session session = sessionFactory.openSession()) {
            return (int) session.save(image);
        }
    }

    @Override
    public void delete(AddonImage image) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(image);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(AddonImage image) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(image);
            session.getTransaction().commit();
        }
    }

    @Override
    public AddonImage getImageById(int image_id) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery("from AddonImage where imageId = :image_id", AddonImage.class);
            query.setParameter("image_id", image_id);
            List<AddonImage> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Image", image_id);
            }
            return result.get(0);
        }
    }
}
