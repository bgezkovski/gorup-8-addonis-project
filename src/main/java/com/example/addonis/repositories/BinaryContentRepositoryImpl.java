package com.example.addonis.repositories;

import com.example.addonis.exceptions.EntityNotFoundException;
import com.example.addonis.models.AddonBinary;
import com.example.addonis.repositories.contracts.BinaryContentRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class BinaryContentRepositoryImpl implements BinaryContentRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public BinaryContentRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void saveBinaryContent(AddonBinary binary) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(binary);
            session.getTransaction().commit();
        }
    }

    @Override
    public AddonBinary getBinaryContent(int id) {
        try (Session session = sessionFactory.openSession()) {
            AddonBinary addonBinary = session.get(AddonBinary.class, id);
            if (addonBinary == null) {
                throw new EntityNotFoundException("Addon binary for addon with id " + id + " not found");
            }
            return addonBinary;
        }
    }

    @Override
    public void deleteBinaryByAddonId(int addon_id) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.createNativeQuery("delete from addons_binary where addon_id =:addon_id")
                    .setParameter("addon_id", addon_id).executeUpdate();
            transaction.commit();
        }
    }
}
