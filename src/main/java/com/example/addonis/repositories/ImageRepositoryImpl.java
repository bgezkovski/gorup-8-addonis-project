package com.example.addonis.repositories;

import com.example.addonis.exceptions.EntityNotFoundException;
import com.example.addonis.models.Image;
import com.example.addonis.repositories.contracts.ImageRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ImageRepositoryImpl implements ImageRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ImageRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int create(Image image) {
        try (Session session = sessionFactory.openSession()) {
            return (int) session.save(image);
        }
    }

    @Override
    public void delete(Image image) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(image);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Image image) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(image);
            session.getTransaction().commit();
        }
    }

    @Override
    public Image getImageById(int image_id) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery("from Image where imageId = :image_id", Image.class);
            query.setParameter("image_id", image_id);
            List<Image> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Image", image_id);
            }
            return result.get(0);
        }
    }
}
