package com.example.addonis.repositories;

import com.example.addonis.models.Rating;
import com.example.addonis.repositories.contracts.RatingRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class RatingRepositoryImpl implements RatingRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public RatingRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Rating rating) {
        try (Session session = sessionFactory.openSession()) {
            session.save(rating);
        }
    }

    @Override
    public void delete(Rating rating) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(rating);
            session.getTransaction().commit();
        }
    }

    @Override
    public Map<Integer, Double> getMappedRating() {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            Query query = session.createNativeQuery("select avg(rating), addon_id " +
                    "from addons_ratings group by addon_id");
            transaction.commit();
            Map<Integer, Double> result = new HashMap<>();
            List<Object[]> rows = query.list();
            for (Object[] row : rows) {
                result.put(Integer.parseInt(row[1].toString()), Double.parseDouble(row[0].toString()));
            }
            return result;

        }
    }

    @Override
    public List<Rating> getRatingsByAddonId(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createNativeQuery("select * from addons_ratings where addon_id =:addon_id", Rating.class);
            query.setParameter("addon_id", id);
            return query.list();
        }
    }

    @Override
    public List<Rating> getRatingsByUserId(int user_id) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createNativeQuery("select * from addons_ratings where user_id =:user_id");
            query.setParameter("user_id", user_id);
            return query.list();
        }
    }

    @Override
    public boolean ratingDoesNotExists(int user_id, int addon_id) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createNativeQuery("select * from addons_ratings where user_id =:user_id and addon_id =:addon_id");
            query.setParameter("user_id", user_id);
            query.setParameter("addon_id", addon_id);
            return (query.list().isEmpty());
        }
    }
}