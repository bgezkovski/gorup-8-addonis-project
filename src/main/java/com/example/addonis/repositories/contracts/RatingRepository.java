package com.example.addonis.repositories.contracts;

import com.example.addonis.models.Rating;

import java.util.List;
import java.util.Map;

public interface RatingRepository {

    void create(Rating rating);

    void delete(Rating rating);

    public Map<Integer, Double> getMappedRating();

    List<Rating> getRatingsByAddonId(int id);


    List<Rating> getRatingsByUserId(int user_id);

    boolean ratingDoesNotExists(int user_id, int addon_id);
}