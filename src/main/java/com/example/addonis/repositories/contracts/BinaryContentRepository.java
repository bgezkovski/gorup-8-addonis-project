package com.example.addonis.repositories.contracts;

import com.example.addonis.models.AddonBinary;

public interface BinaryContentRepository {

    void saveBinaryContent(AddonBinary binary);

    AddonBinary getBinaryContent(int id);

    void deleteBinaryByAddonId(int addon_id);
}
