package com.example.addonis.repositories.contracts;

import com.example.addonis.models.Image;

public interface ImageRepository {
    int create(Image image);

    void delete(Image image);

    void update(Image image);

    Image getImageById(int image_id);

}
