package com.example.addonis.repositories.contracts;

import com.example.addonis.exceptions.DuplicateEntityException;
import com.example.addonis.models.Addon;
import com.example.addonis.models.FilterOptions;
import com.example.addonis.models.dtos.AddonFilterDto;

import java.util.List;
import java.util.Optional;

public interface AddonRepository {

    /**
     * Gets a certain addon by id and returns that addon.
     *
     * @param id addon's id
     * @return addon object
     * @throws DuplicateEntityException
     */
    Addon getById(int id);

    Addon getByName(String name);

    /**
     * Creates a List of all the addons in the database
     *
     * @return query list of all addons
     */
    List<Addon> getAddons();

    List<Addon> getAddons(Optional<String> search);

    /**
     * Creates an addon through a sessionFactory
     *
     * @param addon object
     */
    int create(Addon addon);

    /**
     * Updates an addon through a sessionFactory
     *
     * @param addon object
     */
    void update(Addon addon);

    /**
     * Delete the addon with the given id
     *
     * @param id addon's id
     */
    void delete(int id);

    List<Addon> filter(AddonFilterDto addonFilterDto);

    List<Addon> get(FilterOptions filterOptions);

    List<Addon> getMostRecent();

    List<Addon> getMostDownloaded();

    List<Addon> getFeatured();

    List<Addon> getAddonsByUserId(int user_id);
}