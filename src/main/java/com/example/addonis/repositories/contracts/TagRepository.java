package com.example.addonis.repositories.contracts;

import com.example.addonis.exceptions.DuplicateEntityException;
import com.example.addonis.exceptions.EntityNotFoundException;
import com.example.addonis.models.Tag;

import java.util.List;

public interface TagRepository {

    /**
     * Creates a List of all the tags in the database
     *
     * @return query list of all tags
     */
    List<Tag> getTags();

    /**
     * Gets a certain tag by id and returns that tag.
     *
     * @param id tag's id
     * @return tag object
     * @throws EntityNotFoundException
     */
    Tag getById(int id);

    /**
     * Gets a certain tag by name and returns that tag.
     *
     * @param name tag's name
     * @return tag object
     * @throws DuplicateEntityException
     */

    Tag getByName(String name);

    /**
     * Creates a tag through a sessionFactory
     *
     * @param tag object
     */
    void create(Tag tag);

    /**
     * Delete the tag with the given id
     *
     * @param id
     */
    void delete(int id);

    void deleteTagsByAddonId(int addon_id);
}
