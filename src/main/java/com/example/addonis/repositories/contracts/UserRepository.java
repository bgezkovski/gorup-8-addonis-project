package com.example.addonis.repositories.contracts;

import com.example.addonis.models.Role;
import com.example.addonis.models.User;
import com.example.addonis.models.dtos.UserFilterDto;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    List<User> getUsers(Optional<String> search);

    List<User> get(UserFilterDto userFilterDto);

    User getById(int id);

    User getByUsername(String username);

    User getByEmail(String email);

    User getByPhone(String phone);

    int getCount();

    void create(User user);

    void update(User user);

    void delete(int id);

    Role getRoleById(int id);

    List<Role> getRoles();

    List<String> getAllEmails();

    List<String> getAllPhoneNumbers();

    List<String> getAllUsernames();
}
