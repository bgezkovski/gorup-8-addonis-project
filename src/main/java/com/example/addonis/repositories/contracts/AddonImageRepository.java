package com.example.addonis.repositories.contracts;

import com.example.addonis.models.AddonImage;

public interface AddonImageRepository {

    int create(AddonImage image);

    void delete(AddonImage image);

    void update(AddonImage image);

    AddonImage getImageById(int image_id);
}
