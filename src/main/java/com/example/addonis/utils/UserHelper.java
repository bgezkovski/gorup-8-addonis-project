package com.example.addonis.utils;

public interface UserHelper {

     void checkUsernameExists(String username);

     void checkEmailExists(String email);

     void checkPhoneExists(String phone);

     void checkUsernameExists(String username, int id);

     void checkEmailExists(String email, int id);

     void checkPhoneExists(String phone, int id);
}
