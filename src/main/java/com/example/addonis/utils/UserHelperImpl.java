package com.example.addonis.utils;

import com.example.addonis.exceptions.EmailAlreadyExistsException;
import com.example.addonis.exceptions.EntityNotFoundException;
import com.example.addonis.exceptions.PhoneAlreadyExistsException;
import com.example.addonis.exceptions.UsernameAlreadyExistsException;
import com.example.addonis.models.User;
import com.example.addonis.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserHelperImpl implements UserHelper {
    private static final int NOT_EXISTING_ID = -1;
    UserRepository userRepository;

    @Autowired
    public UserHelperImpl(UserRepository userRepository) {

        this.userRepository = userRepository;
    }

    @Override
    public void checkUsernameExists(String username) {
        checkUsernameExists(username, NOT_EXISTING_ID);
    }

    @Override
    public void checkEmailExists(String email) {
        checkEmailExists(email, NOT_EXISTING_ID);
    }

    @Override
    public void checkPhoneExists(String phone) {
        checkPhoneExists(phone, NOT_EXISTING_ID);
    }

    @Override
    public void checkUsernameExists(String username, int id) {
        boolean usernameExists = true;
        try {
            User userByUsername = userRepository.getByUsername(username);
            if (id == userByUsername.getId()) {
                usernameExists = false;
            }
        } catch (EntityNotFoundException e) {
            usernameExists = false;
        }
        if (usernameExists) {
            throw new UsernameAlreadyExistsException("User", "username", username);
        }
    }

    @Override
    public void checkEmailExists(String email, int id) {
        boolean emailExists = true;
        try {
            User userByEmail = userRepository.getByEmail(email);
            if (id == userByEmail.getId()) {
                emailExists = false;
            }
        } catch (EntityNotFoundException e) {
            emailExists = false;
        }
        if (emailExists) {
            throw new EmailAlreadyExistsException("User", "email", email);
        }
    }

    @Override
    public void checkPhoneExists(String phone, int id) {
        boolean phoneExists = true;
        try {
            User userByPhone = userRepository.getByPhone(phone);
            if (id == userByPhone.getId()) {
                phoneExists = false;
            }
        } catch (EntityNotFoundException e) {
            phoneExists = false;
        }
        if (phoneExists) {
            throw new PhoneAlreadyExistsException("User", "phone", phone);
        }
    }
}
