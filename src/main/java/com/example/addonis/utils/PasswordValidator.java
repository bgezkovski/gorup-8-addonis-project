package com.example.addonis.utils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordValidator implements ConstraintValidator<Password, String> {

    @Override
    public void initialize(Password constraintAnnotation) {
    }

    @Override
    public boolean isValid(String object, ConstraintValidatorContext constraintContext) {
        if (object == null) {
            return false;
        }

        if (object.toCharArray().length >= 8) {
            Pattern capitalLetter = Pattern.compile("[A-Z]");
            Pattern digit = Pattern.compile("[0-9]");
            Pattern special = Pattern.compile("[!@#$%&*()_+=|<>?{}\\[\\]~-]");
            Matcher hasCapitalLetter = capitalLetter.matcher(object);
            Matcher hasDigit = digit.matcher(object);
            Matcher hasSpecial = special.matcher(object);
            return hasCapitalLetter.find() && hasDigit.find() && hasSpecial.find();
        } else {
            return false;
        }
    }
}
