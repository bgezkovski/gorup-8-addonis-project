package com.example.addonis.controllers.rest;

import com.example.addonis.exceptions.DuplicateEntityException;
import com.example.addonis.exceptions.EntityNotFoundException;
import com.example.addonis.models.Tag;
import com.example.addonis.models.dtos.TagDto;
import com.example.addonis.services.contracts.TagService;
import com.example.addonis.services.mappers.TagMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/tags")
public class TagRestController {

    private final TagService tagService;
    private final TagMapper tagMapper;

    @Autowired
    public TagRestController(TagService tagService,
                             TagMapper tagMapper) {
        this.tagService = tagService;
        this.tagMapper = tagMapper;
    }

    @Operation(summary = "Get all tags", description = "Get all tags in system")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned all tags successfully",
                    content = @Content(mediaType = "application/json"))
    })
    @GetMapping
    public List<Tag> get() {
        return tagService.getTags();
    }

    @Operation(summary = "Get tag by id", description = "Get tag by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully got tag by id",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "Tag whit id not found",
                    content = @Content)
    })
    @GetMapping("/{id}")
    public Tag get(
            @Parameter(description = "Id of tag")
            @PathVariable int id) {
        try {
            return tagService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Operation(summary = "Create tag", description = "Create new tag, takes Tag Data Transfer Object")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully created new tag",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "409", description = "Tag already exists in system",
                    content = @Content)
    })
    @PostMapping
    public Tag create(
            @Parameter(description = "Tag Data Transfer Object")
            @Valid @RequestBody TagDto tagDto) {
        try {
            Tag tag = tagMapper.tagDtoToTag(tagDto);
            tagService.create(tag);
            return tag;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @Operation(summary = "Delete tag", description = "Deletes a tag using tag id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Tag deleted successfully",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Tag not found",
                    content = @Content)
    })
    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            tagService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
