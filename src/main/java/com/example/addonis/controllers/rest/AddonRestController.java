package com.example.addonis.controllers.rest;

import com.example.addonis.exceptions.BinaryFileException;
import com.example.addonis.exceptions.DuplicateEntityException;
import com.example.addonis.exceptions.EntityNotFoundException;
import com.example.addonis.models.Addon;
import com.example.addonis.models.AddonImage;
import com.example.addonis.models.Rating;
import com.example.addonis.models.User;
import com.example.addonis.models.dtos.AddonDto;
import com.example.addonis.models.dtos.AddonDtoOut;
import com.example.addonis.models.dtos.RatingDto;
import com.example.addonis.services.contracts.AddonImageService;
import com.example.addonis.services.contracts.AddonService;
import com.example.addonis.services.contracts.RatingService;
import com.example.addonis.services.contracts.UserService;
import com.example.addonis.services.mappers.AddonMapper;
import com.example.addonis.services.mappers.RatingMapper;
import com.example.addonis.utils.ImageUtility;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/addons")
public class AddonRestController {

    private final AddonService service;
    private final UserService userService;

    private final AddonMapper addonMapper;

    private final RatingMapper ratingMapper;

    private final AddonImageService addonImageService;

    private final RatingService ratingService;


    @Autowired
    public AddonRestController(AddonService addonService,
                               UserService userService,
                               AddonMapper addonMapper,
                               RatingMapper ratingMapper,
                               AddonImageService addonImageService,
                               RatingService ratingService) {
        this.service = addonService;
        this.userService = userService;
        this.addonMapper = addonMapper;
        this.ratingMapper = ratingMapper;
        this.addonImageService = addonImageService;
        this.ratingService = ratingService;
    }

    @Operation(summary = "Get list of addons",
            description = "Retrieves a all addons from the system also allows for querying")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved list of addons",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "409", description = "Wrong search parameter",
                    content = @Content)
    })
    @GetMapping
    public List<Addon> get(
            @Parameter(description = "search query")
            @RequestParam(required = false) Optional<String> search) {
        try {
            return service.getAddons(search);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @Operation(summary = "Get addon",
            description = "Retrieves a single addon by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Addon retrieved successfully",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "No addon with id found",
                    content = @Content)
    })
    @GetMapping("/{id}")
    public AddonDtoOut get(
            @Parameter(description = "id of addon")
            @PathVariable int id) {
        try {
            Addon addon = service.getById(id);
            return addonMapper.addonToAddonDtoOut(addon);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Operation(summary = "Add tag to addon",
            description = "Add a tag to an Addon, if the tag doesnt exist(globally) it creates a new," +
                    " tags must be Strings and have a min 4 and max 15 characters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Tag added successfully to addon",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Addon with id not found",
                    content = @Content),
            @ApiResponse(responseCode = "409", description = "Tag already applied to this addon",
                    content = @Content)
    })
    @PutMapping("/addtag/{addonId}")
    public void addTag(
            @Parameter(description = "Id of addon")
            @PathVariable int addonId,
            @Parameter(description = "Name of tag")
            @RequestParam String tagName) {
        try {
            service.addTag(addonId, tagName);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @Operation(summary = "Remove tag from addon",
            description = "Remove a tag whit name from an addon by addon id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Tag removed successfully from addon",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Addon or tag not found",
                    content = @Content)
    })
    @DeleteMapping("/removetag/{addonId}")
    public void removeTag(
            @Parameter(description = "id of addon to remove tag from")
            @PathVariable int addonId,
            @Parameter(description = "Name of tag to remove")
            @RequestParam String tagName) {
        try {
            service.removeTag(addonId, tagName);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Operation(summary = "Rate addon",
            description = "Rate addon using addon id takes rating Data Transfer Object")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Rated addon successfully",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Addon not found",
                    content = @Content)
    })
    @PostMapping("/rate/{addonId}")
    public Addon rateAddon(
            @Parameter(description = "rating")
            @RequestBody RatingDto ratingDto,
            @Parameter(description = "addon id")
            @PathVariable int addonId,
            Principal principal) {
        try {
            Rating rating = ratingMapper.mapRatingDtoToRating(addonId, principal.getName(), ratingDto);
            ratingService.addRatingToAddon(rating);

            return service.getById(addonId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Operation(summary = "Create addon",
            description = "Creates new addon, takes Addon Data Transfer Object and logged in user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Addon created successfully",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "409", description = "Addon already exists",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Validation error",
                    content = @Content)
    })
    @PostMapping
    public AddonDtoOut create(
            @Parameter(description = "Addon Data Transfer Object")
            @Validated @RequestBody AddonDto addonDto,
            @Parameter(description = "Logged in user")
            Principal principal) {
        try {
            User user = userService.getByUsername(principal.getName());
            Addon addon = addonMapper.addonDtoToAddon(addonDto, user);
            service.create(addon, addonDto.getTag());
            return addonMapper.addonToAddonDtoOut(addon);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @Operation(summary = "Update addon",
            description = "Update an existing addon using Id, takes Addon Data Transfer Object and logged in user " +
                    ", user must be owner of addon or admin")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Addon updated successfully",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "Addon not found",
                    content = @Content),
            @ApiResponse(responseCode = "409", description = "Addon already exists ??",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Validation error",
                    content = @Content)
    })
    @PutMapping("/{id}")
    public Addon update(
            @Parameter(description = "Id of addon to update")
            @PathVariable int id,
            @Parameter(description = "Addon Data Transfer Object")
            @Validated @RequestBody AddonDto addonDto) {
        try {
            Addon addonToUpdate = service.getById(id);
            Addon updatedAddon = addonMapper.addonDtoToAddon(addonDto, addonToUpdate.getCreator());
            updatedAddon.setAddonId(id);
            service.update(updatedAddon);
            return updatedAddon;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @Operation(summary = "Delete addon", description = "Delete addon by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Addon deleted successfully",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Addon not found",
                    content = @Content)
    })
    @DeleteMapping("/{id}")
    public void delete(
            @Parameter(description = "Id of addon to delete")
            @PathVariable int id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Operation(summary = "Get image", description = "Get image by addon id, returns image file")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Retrieved image successfully",
                    content = @Content(mediaType = "image/png")),
            @ApiResponse(responseCode = "404", description = "No image found for addon id, addon most likely does not exist",
                    content = @Content)
    })
    @GetMapping("image/{addon_id}")
    public ResponseEntity<byte[]> getImage(
            @Parameter(description = "Id of addon")
            @PathVariable("addon_id") int addonId) {

        try {
            final AddonImage dbImage = service.getById(addonId).getImage();

            return ResponseEntity
                    .ok()
                    .contentType(MediaType.valueOf(dbImage.getType()))
                    .body(ImageUtility.decompressImage(dbImage.getImage()));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Operation(summary = "Upload image", description = "Upload image for addon")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Image uploaded successfully",
                    content = @Content),
            @ApiResponse(responseCode = "406", description = "Wrong file type",
                    content = @Content)
    })
    @PostMapping("image/")
    public ResponseEntity<ImageUploadResponse> uploadNewImage(
            @Parameter(description = "Image to upload")
            @RequestParam("image") MultipartFile file) {

        try {
            addonImageService.create(file);

            return ResponseEntity.status(HttpStatus.OK)
                    .body(new ImageUploadResponse("Image uploaded successfully: " +
                            file.getOriginalFilename()));
        } catch (BinaryFileException e) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, e.getMessage());
        }
    }

    @Operation(summary = "Update image", description = "Update image for addon using addon id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Image uploaded successfully",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Addon not found",
                    content = @Content),
            @ApiResponse(responseCode = "406", description = "Wrong file type",
                    content = @Content)
    })
    @PutMapping("image/{addon_id}")
    public ResponseEntity<ImageUploadResponse> update(
            @Parameter(description = "Addon id")
            @PathVariable("addon_id") int addonId,
            @Parameter(description = "New image for addon")
            @RequestParam("image") MultipartFile file) {
        try {
            addonImageService.update(file, addonId);
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new ImageUploadResponse("Image updated successfully: " +
                            file.getOriginalFilename()));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (BinaryFileException e) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, e.getMessage());
        }
    }

    @Operation(summary = "Delete image", description = "Delete image from addon")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Image deleted successfully",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Addon not found",
                    content = @Content)
    })
    @DeleteMapping("image/{addon_id}")
    public ResponseEntity<ImageUploadResponse> deleteImage(
            @Parameter(description = "Id of addon")
            @PathVariable("addon_id") int addonId) {
        try {
            final AddonImage dbImage = service.getById(addonId).getImage();
            if (dbImage.getImageId() == 1) {
                return ResponseEntity.status(HttpStatus.OK)
                        .body(new ImageUploadResponse("Default image can not be deleted: " + dbImage.getName()));
            } else {
                Addon addon = service.getById(addonId);
                addon.setImage(addonImageService.getImageById(1));
                service.update(addon);
                addonImageService.delete(dbImage);
                return ResponseEntity.status(HttpStatus.OK)
                        .body(new ImageUploadResponse("Image deleted successfully: " +
                                dbImage.getName()));
            }
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}
