package com.example.addonis.controllers.rest;

import com.example.addonis.exceptions.BinaryFileException;
import com.example.addonis.services.contracts.AddonBinaryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/binary")
public class BinaryContentRestController {

    private final AddonBinaryService service;

    @Autowired
    public BinaryContentRestController(AddonBinaryService service) {
        this.service = service;
    }

    @Operation(summary = "Upload binary file", description = "Upload a binary file for addon")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Binary file uploaded successfully",
                    content = @Content),
            @ApiResponse(responseCode = "406", description = "Error uploading file",
                    content = @Content)
    })
    @PostMapping("/{id}")
    void UploadContent(
            @Parameter(description = "Binary file to upload")
            @RequestParam("file") MultipartFile file,
            @Parameter(description = "Addon id")
            @PathVariable int id) {
        try {
            service.uploadBinary(file, id);
        } catch (BinaryFileException e) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, e.getMessage());
        }
    }

    @Operation(summary = "Get binary file", description = "Get binary file for addon")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "File retrieved successfully",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "File not found",
                    content = @Content)
    })
    @GetMapping(value = "/{addonId}", produces = MediaType.APPLICATION_CBOR_VALUE)
    Resource downloadBinary(@PathVariable int addonId) {
        try {
            return new ByteArrayResource(service.getBinary(addonId));
        } catch (BinaryFileException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
