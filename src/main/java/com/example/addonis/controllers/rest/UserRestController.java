package com.example.addonis.controllers.rest;

import com.example.addonis.exceptions.*;
import com.example.addonis.models.Image;
import com.example.addonis.models.User;
import com.example.addonis.models.dtos.RegistrationValidationDto;
import com.example.addonis.models.dtos.UserDto;
import com.example.addonis.models.dtos.UserDtoOut;
import com.example.addonis.models.dtos.UserFilterDto;
import com.example.addonis.services.contracts.ImageService;
import com.example.addonis.services.contracts.UserService;
import com.example.addonis.services.mappers.UserMapper;
import com.example.addonis.utils.ImageUtility;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.hibernate.hql.internal.ast.QuerySyntaxException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private final UserService userService;
    private final UserMapper userMapper;

    private final ImageService imageService;


    @Autowired
    public UserRestController(UserService userService,
                              UserMapper userMapper,
                              ImageService imageService) {

        this.userMapper = userMapper;
        this.userService = userService;
        this.imageService = imageService;
    }

    @Operation(summary = "Get all users or filter user list",
            description = "Gets all users from system also allows querying")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found user",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", description = "No users found for query",
                    content = @Content)
    })
    @GetMapping
    public List<UserDtoOut> getAll(
            @Parameter(description = "String to search by")
            @RequestParam(required = false) Optional<String> search) {
        //TODO Authentication Check Admin rights - Spring Security
        List<UserDtoOut> query = userService.getUsers(search).stream().
                map(u -> userMapper.userToUserDtoOut(u)).
                collect(Collectors.toList());

        if (query.size() > 0)
            return query;
        else
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No users found for query: " + search);
    }

    @Operation(summary = "Get user",
            description = "Gets a user by user id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found user",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", description = "User with this id does not exist",
                    content = @Content
            )
    })
    @GetMapping("/{id}")
    public UserDtoOut get(
            @Parameter(description = "id of user")
            @PathVariable int id) {
        try {
            User user = userService.getById(id);
            return userMapper.userToUserDtoOut(user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Operation(summary = "Filter users list",
            description = "Retrieves user list and filters with supplied filter parameters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found users according to criteria",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "400", description = "Bad query parameter",
                    content = @Content
            )}
    )
    @GetMapping("/filter")
    public List<UserDtoOut> filter(
            @Parameter(description = "Filter by username")
            @RequestParam(required = false) Optional<String> username,
            @Parameter(description = "Filter by role name")
            @RequestParam(required = false) Optional<String> roleName,
            @Parameter(description = "Filter by email")
            @RequestParam(required = false) Optional<String> email,
            @Parameter(description = "Filter by phone number")
            @RequestParam(required = false) Optional<String> phone,
            @Parameter(description = "Sort by field(username, emil etc.)")
            @RequestParam(required = false) Optional<String> sortBy,
            @Parameter(description = "Sort order asc - ascending | desc - descending")
            @RequestParam(required = false) Optional<String> sortOrder) {
        try {
            UserFilterDto userFilterDto = userMapper.paramsToFilterDto(username,
                    roleName, email, phone, sortBy, sortOrder);
            return userService.getUsers(userFilterDto)
                    .stream()
                    .map(u -> userMapper.userToUserDtoOut(u))
                    .collect(Collectors.toList());
        } catch (QuerySyntaxException | IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid query");
        }
    }

    @Operation(summary = "Get logged in user count",
            description = "Retrieve the amount of users logged in at the moment")
    @GetMapping("/count")
    public int getCount() {
        return userService.getCount();
    }

    @Operation(summary = "Create new user",
            description = "Creates a new user in the system accepts a User Data Transfer Object")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "New user created successfully",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "409", description = "Duplicate (exists) username/phone/email",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Validation error",
                    content = @Content)
    })
    @PostMapping
    @CrossOrigin(origins = "*")
    public UserDtoOut create(@Valid @RequestBody UserDto userDto) {
        try {
            User user = userMapper.userDtoToUser(userDto);
            userService.create(user);
            return userMapper.userToUserDtoOut(user);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @Operation(summary = "Update user",
            description = "Updates user info, takes a User Data Transfer Object ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful update of user",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "User with id not found, make sure user with id exists",
                    content = @Content),
            @ApiResponse(responseCode = "409", description = "Duplicate (exists) username/phone/email",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Validation error",
                    content = @Content)
    })
    @PutMapping("/{id}")
    public UserDtoOut update(
            @Parameter(description = "id of user to update")
            @PathVariable int id,
            @Valid @RequestBody UserDto userDto) {
        try {
            User userToUpdate = userService.getById(id);
            User userUpdated = userMapper.userDtoToUser(userDto, userToUpdate.getId());
            userService.update(userUpdated);
            return userMapper.userToUserDtoOut(userToUpdate);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @Operation(summary = "Promote user",
            description = "Changes user role to ADMIN")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully promoted user to admin",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "User with id not found, make sure user with id exists",
                    content = @Content),
            @ApiResponse(responseCode = "409", description = "Not sure",
                    content = @Content)
    })
    @PutMapping("/promote/{id}/admin")
    public UserDtoOut promoteToAdmin(
            @Parameter(description = "id of user to update")
            @PathVariable int id) {
        try {
            User userToPromote = userService.getById(id);
            userService.promoteToAdmin(userToPromote);
            return userMapper.userToUserDtoOut(userToPromote);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @Operation(summary = "Block user",
            description = "Changes user blocked state to TRUE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User blocked successfully",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "User with id not found, make sure user with id exists",
                    content = @Content),
            @ApiResponse(responseCode = "409", description = "Not sure",
                    content = @Content)
    })
    @PutMapping("/block/{id}")
    public UserDtoOut blockUser(
            @Parameter(description = "id of user to block")
            @PathVariable int id) {
        try {
            User userToBlock = userService.getById(id);
            return userMapper.userToUserDtoOut(userService.blockUser(userToBlock));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @Operation(summary = "Unblock user",
            description = "Changes user blocked state to FALSE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User unblocked successfully",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "User with id not found, make sure user with id exists",
                    content = @Content),
            @ApiResponse(responseCode = "409", description = "Not sure",
                    content = @Content)
    })
    @PutMapping("/unblock/{id}")
    public UserDtoOut unblockUser(
            @Parameter(description = "id of user to unblocked")
            @PathVariable int id) {
        try {
            User userToUnblock = userService.getById(id);
            return userMapper.userToUserDtoOut(userService.unblockUser(userToUnblock));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @Operation(summary = "Delete user",
            description = "Deletes a user using user id only owners and admins can perform this action")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User deleted successfully",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "User with id not found, make sure user with id exists",
                    content = @Content)
    })
    @DeleteMapping("/{id}")
    public void delete(
            @Parameter(description = "id of user to delete")
            @PathVariable int id) {
        try {
            userService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Operation(summary = "Activate user",
            description = "Receives validation code from validation email and sets user active state to TRUE")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User activated successfully",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Code expired send new code",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "")
    })
    @PostMapping("/activate/{code}")
    public String activateUser(
            @Parameter(description = "activation code")
            @PathVariable int code) {
        try {
            userService.activateAccount(code);
            return "Account activated";
        } catch (WrongActivationCodeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "code expired send new code");
        }
    }

    @Operation(summary = "Send new activation code",
            description = "Sends a new activation code to the current logged in user's email")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Send new activation code to logged in user",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "User already activated",
                    content = @Content)
    })
    @PostMapping("/activate/new-code")
    public String sendNewActivationCode(Principal principal) {
        try {
            userService.resendActivationCode(principal.getName());
            return "New activation code sent";
        } catch (ForbiddenOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @Operation(summary = "Send invitation email",
            description = "Sends email to unregistered email account with invitation to join the platform")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully sent invitation email",
                    content = @Content),
            @ApiResponse(responseCode = "409", description = "Already sent an email to this email address",
                    content = @Content)
    })
    @GetMapping("/invite/{email}")
    public String inviteFriend(Principal principal,
                               @Parameter(description = "email address to send to")
                               @PathVariable String email) {
        try {
            userService.inviteFriend(principal.getName(), email);
            return "Sent invitation link";
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @Operation(summary = "Get registered users data",
            description = "used for frontend ui, internal developers ONLY")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "All data retrieved successfully",
                    content = @Content)
    })
    @GetMapping("/register_data")
    @CrossOrigin(origins = "*")
    public RegistrationValidationDto getEmails() {
        return new RegistrationValidationDto(
                userService.getAllEmails(),
                userService.getAllUsernames(),
                userService.getAllPhoneNumbers()
        );
    }

    @Operation(summary = "Get image",
            description = "Get image by user id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Image retrieved successfully",
                    content = @Content(mediaType = "image/png")),
            @ApiResponse(responseCode = "404", description = "No image found for user id, user most likely does not exist",
                    content = @Content)
    })
    @GetMapping("image/{user_id}")
    public ResponseEntity<byte[]> getImage(
            @Parameter(description = "id of user")
            @PathVariable("user_id") int userId) {

        try {
            final Image dbImage = userService.getById(userId).getImage();

            return ResponseEntity
                    .ok()
                    .contentType(MediaType.valueOf(dbImage.getType()))
                    .body(ImageUtility.decompressImage(dbImage.getImage()));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Operation(summary = "Upload image for user with id",
            description = "Upload a new image for user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Image uploaded successfully",
                    content = @Content),
            @ApiResponse(responseCode = "406", description = "Task failed successfully",
                    content = @Content)
    })
    @PostMapping("image/")
    public ResponseEntity<ImageUploadResponse> uploadNewImage(
            @Parameter(description = "image to upload")
            @RequestParam("image") MultipartFile file) {
        try {
            imageService.create(file);

            return ResponseEntity.status(HttpStatus.OK)
                    .body(new ImageUploadResponse("Image uploaded successfully: " +
                            file.getOriginalFilename()));
        } catch (BinaryFileException e) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, e.getMessage());
        }
    }

    @Operation(summary = "Update image for user by id",
            description = "Updates an image in the system for a specific user id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Image updated successfully",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "User with id not found",
                    content = @Content),
            @ApiResponse(responseCode = "406", description = "Wrong file type",
                    content = @Content)
    })
    @PutMapping("image/{user_id}")
    public ResponseEntity<ImageUploadResponse> update(
            @Parameter(description = "id of user")
            @PathVariable("user_id") int userId,
            @RequestParam("image") MultipartFile file) {
        try {
            imageService.update(file, userId);

            return ResponseEntity.status(HttpStatus.OK)
                    .body(new ImageUploadResponse("Image updated successfully: " +
                            file.getOriginalFilename()));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (BinaryFileException e) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, e.getMessage());
        }
    }

    @Operation(summary = "Delete image by user id",
            description = "Deletes an image for a specific users and sets his photo to the default one")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Image deleted successfully",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "User with id not found",
                    content = @Content)
    })
    @DeleteMapping("image/{user_id}")
    public ResponseEntity<ImageUploadResponse> deleteImage(
            @Parameter(description = "id of user")
            @PathVariable("user_id") int userId) {
        try {
            final Image dbImage = userService.getById(userId).getImage();
            if (dbImage.getImageId() == 1) {
                return ResponseEntity.status(HttpStatus.OK)
                        .body(new ImageUploadResponse("Default image can not be deleted: " + dbImage.getName()));
            } else {
                User user = userService.getById(userId);
                user.setImage(imageService.getImageById(1));
                userService.update(user);
                imageService.delete(dbImage);
                return ResponseEntity.status(HttpStatus.OK)
                        .body(new ImageUploadResponse("Image deleted successfully: " +
                                dbImage.getName()));
            }
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
