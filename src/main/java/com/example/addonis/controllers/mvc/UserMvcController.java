package com.example.addonis.controllers.mvc;

import com.example.addonis.exceptions.*;
import com.example.addonis.models.Image;
import com.example.addonis.models.User;
import com.example.addonis.models.dtos.UserDto;
import com.example.addonis.models.dtos.UserFilterDto;
import com.example.addonis.models.dtos.UserFilterDtoEmpty;
import com.example.addonis.services.contracts.ImageService;
import com.example.addonis.services.contracts.UserService;
import com.example.addonis.services.mappers.UserMapper;
import com.example.addonis.utils.AuthenticationHelper;
import com.example.addonis.utils.ImageUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


@Controller
@RequestMapping("/users")
public class UserMvcController extends BaseView {

    private final UserService userService;
    private final UserMapper userMapper;

    private final ImageService imageService;

    private final AuthenticationHelper authHelper;


    @Autowired
    public UserMvcController(UserService userService,
                             UserMapper userMapper,
                             ImageService imageService,
                             AuthenticationHelper authHelper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.imageService = imageService;
        this.authHelper = authHelper;
    }

    @ModelAttribute("roles")
    public List<String> populateRoles() {
        return userService.getRoles().stream().map(role -> role.getName()).collect(Collectors.toList());
    }

    @ModelAttribute("sortParams")
    public List<String> populateSortParams() {
        return new ArrayList<>(List.of("username", "roleName", "email", "phone"));
    }

    @ModelAttribute("sortOrders")
    public List<String> populateSortOrder() {
        return new ArrayList<>(List.of("asc", "desc"));
    }

    @GetMapping("/{user_id}")
    public String showSingleUser(@PathVariable int user_id, Model model, Principal principal) {
        try {
            addAttributesToModel(model, principal);
            User user = userService.getById(user_id);
            model.addAttribute("user", user);
            model.addAttribute("imgUtil", new ImageUtility());
            return "UserView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @RequestMapping(value = "/{user_id}", method = RequestMethod.POST)
    public String imageUpload(@PathVariable("user_id") int userId,
                              @RequestParam("file") MultipartFile file,
                              Model model) {

        try {
            imageService.update(file, userId);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (BinaryFileException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/users/{user_id}";
        }
        return "redirect:/users/{user_id}";
    }

    @RequestMapping(value = "/{user_id}/delete-image", method = RequestMethod.GET)
    public String imageDelete(@PathVariable("user_id") int userId, Model model) {

        try {
            User user = userService.getById(userId);
            if (user.getImage().getImageId() != 1) {
                Image imageToDelete = imageService.getImageById(user.getImage().getImageId());
                user.setImage(imageService.getImageById(1));
                userService.update(user);
                imageService.delete(imageToDelete);
            }

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
        return "redirect:/users/{user_id}";
    }


    @GetMapping("")
    public String pageUsers(
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size,
            HttpSession session,
            Principal principal) {
        List<User> users;
        if (session.getAttribute("userFilter") == null) {
            UserFilterDto userFilterDto = new UserFilterDto();
            users = userService.getUsers(userFilterDto);
        } else {
            users = userService.getUsers((UserFilterDto) session.getAttribute("userFilter"));
        }
        addAttributesToModel(model, principal);
        model.addAttribute("users", users);
        model.addAttribute("userFilterDto", new UserFilterDtoEmpty());

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Page<User> usersPage = userService.findPaginated(PageRequest.of(currentPage - 1, pageSize), users);

        model.addAttribute("usersPage", usersPage);
        model.addAttribute("imgUtil", new ImageUtility());

        int totalPages = usersPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        int previousPage = (currentPage <= 1) ? 1 : (currentPage - 1);
        int nextPage = (currentPage >= totalPages) ? (totalPages) : (currentPage + 1);
        model.addAttribute("previousPage", previousPage);
        model.addAttribute("nextPage", nextPage);
        return "UsersPaged";
    }

    @PostMapping("")
    public String filterUsers(@ModelAttribute UserFilterDto userFilterDto,
                              @RequestParam("page") Optional<Integer> page,
                              @RequestParam("size") Optional<Integer> size,
                              Model model,
                              HttpSession session,
                              Principal principal) {

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        session.setAttribute("userFilter", userFilterDto);

        List<User> users = userService.getUsers(userFilterDto);
        Page<User> usersPage = userService.findPaginated(PageRequest.of(currentPage - 1, pageSize), users);

        model.addAttribute("users", users);
        model.addAttribute("users", userService.getUsers(userFilterDto));
        model.addAttribute("userFilterDto", new UserFilterDtoEmpty());
        model.addAttribute("usersPage", usersPage);
        model.addAttribute("imgUtil", new ImageUtility());
        addAttributesToModel(model, principal);

        int totalPages = usersPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "UsersPaged";
    }

    @GetMapping("/register")
    public String showNewUserPage(Model model) {
        model.addAttribute("userDto", new UserDto());
        return "register";
    }

    @PostMapping("/register")
    public String createUser(@Valid @ModelAttribute("userDto") UserDto userDto,
                             BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "register";
        }

        try {
            User user = userMapper.userDtoToUser(userDto);
            userService.create(user);
            return "redirect:/login";
        } catch (UsernameAlreadyExistsException e) {
            bindingResult.rejectValue("username", "duplicate_username", e.getMessage());
            return "register";
        } catch (EmailAlreadyExistsException e) {
            bindingResult.rejectValue("email", "duplicate_email", e.getMessage());
            return "register";
        } catch (PhoneAlreadyExistsException e) {
            bindingResult.rejectValue("phone", "duplicate_phone", e.getMessage());
            return "register";
        }
    }


    @GetMapping("/{id}/update")
    public String showUpdateUserPage(@PathVariable int id, Model model, Principal principal) {
        try {
            addAttributesToModel(model, principal);
            User user = userService.getById(id);
            UserDto userDto = userMapper.userToUserDto(user);
            model.addAttribute(userDto);
            return "UserUpdateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{user_id}/update")
    public String updateUser(@PathVariable int user_id,
                             @Valid @ModelAttribute("userDto") UserDto userDto,
                             BindingResult bindingResult,
                             Model model,
                             Principal principal) {
        addAttributesToModel(model, principal);
        if (bindingResult.hasErrors()) {
            return "UserUpdateView";
        }

        try {
            User user = userMapper.userDtoToUser(userDto, user_id);
            userService.update(user);
            return "redirect:/users/{user_id}";
        } catch (UsernameAlreadyExistsException e) {
            bindingResult.rejectValue("username", "duplicate_username", e.getMessage());
            return "UserUpdateView";
        } catch (EmailAlreadyExistsException e) {
            bindingResult.rejectValue("email", "duplicate_email", e.getMessage());
            return "UserUpdateView";
        } catch (PhoneAlreadyExistsException e) {
            bindingResult.rejectValue("phone", "duplicate_phone", e.getMessage());
            return "UserUpdateView";
        }

    }


    @GetMapping("/{id}/promote")
    public String promoteUser(@PathVariable int id) {

        try {
            User user = userService.getById(id);
            userService.promoteToAdmin(user);
            return "redirect:/users/{id}";
        } catch (UnsupportedOperationException e) {
            return "redirect:/users/{id}";
        }
    }

    @GetMapping("/{id}/block")
    public String blockUser(@PathVariable int id) {

        try {
            User user = userService.getById(id);
            userService.blockUser(user);
            return "redirect:/users/{id}";
        } catch (UnsupportedOperationException e) {
            return "redirect:/users/{id}";
        }
    }

    @GetMapping("/{id}/unblock")
    public String unblockUser(@PathVariable int id) {

        try {
            User user = userService.getById(id);
            userService.unblockUser(user);
            return "redirect:/users/{id}";
        } catch (UnsupportedOperationException e) {
            return "redirect:/users/{id}";
        }
    }

    @GetMapping("/{user_id}/delete")
    public String deleteUser(@PathVariable("user_id") int userId, Model model) {
        try {
            SecurityContextHolder.clearContext();
            userService.delete(userId);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }

        return "redirect:/";
    }
}
