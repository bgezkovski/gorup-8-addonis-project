package com.example.addonis.controllers.mvc;

import com.example.addonis.models.User;
import com.example.addonis.models.dtos.AddonFilterDto;
import com.example.addonis.services.contracts.AddonService;
import com.example.addonis.services.contracts.RatingService;
import com.example.addonis.services.contracts.UserService;
import com.example.addonis.services.mappers.UserMapper;
import com.example.addonis.utils.ImageUtility;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/")
public class HomeMvcController extends BaseView {

    private final AddonService addonService;
    private final RatingService ratingService;

    private final UserService userService;

    public HomeMvcController(AddonService addonService,
                             RatingService ratingService,
                             UserService userService) {
        this.addonService = addonService;
        this.ratingService = ratingService;
        this.userService = userService;
    }

    @ModelAttribute("sortParams")
    public List<String> populateSortParams() {
        return new ArrayList<>(List.of("Name, asc", "Name, desc", "Target IDE, asc", "Target IDE, desc"));
    }

    @GetMapping
    public String showHomePage(Model model, Principal principal) {
        addAttributesToModel(model, principal);

        if (isUserLoggedIn(principal)) {
            User user = userService.getByUsername(principal.getName());
            if (!user.isActivated()) {
                return "redirect:/verification/" + user.getUsername();
            }
        }
        model.addAttribute("imgUtil", new ImageUtility());
        model.addAttribute("addons", addonService.getAddons(Optional.empty()));
        model.addAttribute("ratings", ratingService.getMappedRating());
        model.addAttribute("mostRecent", addonService.getMostRecent());
        model.addAttribute("mostDownloaded", addonService.getMostDownloaded());
        model.addAttribute("featured", addonService.getFeatured());
        model.addAttribute("addonFilterDto", new AddonFilterDto());
        return "HomeView";
    }

    @PostMapping("/")
    public String createUser(Model model, Principal principal) {
        return showHomePage(model, principal);
    }

    @PostMapping("/login")
    public String loginError() {
        return "redirect:/?error";
    }

}