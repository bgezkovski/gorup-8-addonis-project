package com.example.addonis.controllers.mvc;

import com.example.addonis.services.contracts.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/about")
public class AboutMvcView extends BaseView {

    private final UserService service;

    public AboutMvcView(UserService service) {
        this.service = service;
    }

    @GetMapping
    public String getAboutPage(Model model, Principal principal) {
        addAttributesToModel(model, principal);
        return "AboutView";
    }

}
