package com.example.addonis.controllers.mvc;

import com.example.addonis.models.User;
import com.example.addonis.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

import java.security.Principal;


public class BaseView {

    @Autowired
    private UserService userService;

    public BaseView() {

    }

    void addAttributesToModel(Model model, Principal principal) {
        model.addAttribute("loggedIn", isUserLoggedIn(principal));
        model.addAttribute("admin", isUserAdmin(principal));
        model.addAttribute("logged_username", loggedUserUsername(principal));
    }

    boolean isUserLoggedIn(Principal principal) {
        return principal != null;
    }

    private boolean isUserAdmin(Principal principal) {
        if (principal != null) {
            User user = userService.getByUsername(principal.getName());
            return user.isAdmin();
        }
        return false;
    }

    private String loggedUserUsername(Principal principal) {
        if (principal != null) {
            return principal.getName();
        }
        return "no_logged_in_user";
    }

}
