package com.example.addonis.controllers.mvc;

import com.example.addonis.exceptions.BinaryFileException;
import com.example.addonis.exceptions.DuplicateEntityException;
import com.example.addonis.exceptions.EntityNotFoundException;
import com.example.addonis.exceptions.UnauthorizedOperationException;
import com.example.addonis.models.*;
import com.example.addonis.models.dtos.AddonDto;
import com.example.addonis.models.dtos.AddonFilterDto;
import com.example.addonis.models.dtos.RatingDto;
import com.example.addonis.models.dtos.TagDto;
import com.example.addonis.services.contracts.*;
import com.example.addonis.services.mappers.AddonMapper;
import com.example.addonis.services.mappers.RatingMapper;
import com.example.addonis.utils.AuthenticationHelper;
import com.example.addonis.utils.ImageUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/addons")
public class AddonMvcController extends BaseView {

    private final AddonService addonService;

    private final TagService tagService;

    private final UserService userService;
    private final AddonMapper addonMapper;

    private final RatingMapper ratingMapper;

    private final RatingService ratingService;

    private final AddonBinaryService binaryService;

    private final AddonImageService addonImageService;

    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public AddonMvcController(AddonService addonService,
                              TagService tagService,
                              UserService userService,
                              AddonMapper addonMapper,
                              RatingMapper ratingMapper,
                              RatingService ratingService,
                              AddonBinaryService binaryService,
                              AddonImageService addonImageService,
                              AuthenticationHelper authenticationHelper) {

        this.addonService = addonService;
        this.tagService = tagService;
        this.userService = userService;
        this.addonMapper = addonMapper;
        this.ratingMapper = ratingMapper;
        this.ratingService = ratingService;
        this.binaryService = binaryService;
        this.addonImageService = addonImageService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("tags")
    public List<Tag> populateTags() {
        return tagService.getTags();
    }

    @ModelAttribute("sortParams")
    public List<String> populateSortParams() {
        return new ArrayList<>(List.of("Name, asc", "Name, desc", "Downloads, asc", "Downloads, desc",
                "Upload date, asc", "Upload date, desc", "Last commit date, asc", "Last commit date, desc"));
    }

    @GetMapping("")
    public String showAllAddons(Model model, Principal principal) {
        addAttributesToModel(model, principal);
        model.addAttribute("addons", addonService.getAddons(Optional.empty()));
        model.addAttribute("imgUtil", new ImageUtility());
        model.addAttribute("addonFilterDto", new AddonFilterDto());
        return "AddonsView";
    }

    @GetMapping("/pending")
    public String showNotApprovedAddons(Model model, Principal principal) {
        addAttributesToModel(model, principal);
        model.addAttribute("addons", addonService.getAddons(Optional.empty()));
        model.addAttribute("imgUtil", new ImageUtility());
        model.addAttribute("addonFilterDto", new AddonFilterDto());
        return "AddonsPendingView";
    }

    @GetMapping("/{id}/approve")
    public String approveAddon(@PathVariable int id, Model model, Principal principal) {

        if (!isUserLoggedIn(principal)) {
            return "redirect:/login";
        }

        if (!userService.getByUsername(principal.getName()).isAdmin()) {
            return "AccessDeniedView";
        }

        try {
            addonService.approveAddon(id);
            addAttributesToModel(model, principal);
            return "redirect:/addons";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{id}/feature")
    public String featureAddon(@PathVariable int id, Model model, Principal principal) {

        if (!isUserLoggedIn(principal)) {
            return "redirect:/login";
        }

        if (!userService.getByUsername(principal.getName()).isAdmin()) {
            return "AccessDeniedView";
        }

        try {
            addonService.featureAddon(id);
            addAttributesToModel(model, principal);
            return "redirect:/";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{id}/removeFeatured")
    public String removeFeatureAddon(@PathVariable int id, Model model, Principal principal) {

        if (!isUserLoggedIn(principal)) {
            return "redirect:/login";
        }

        if (!userService.getByUsername(principal.getName()).isAdmin()) {
            return "AccessDeniedView";
        }

        try {
            addonService.removeFeatureAddon(id);
            addAttributesToModel(model, principal);
            return "redirect:/";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{id}/download")
    public void downloadAddon(@PathVariable int id, HttpServletResponse response) {
        try {
            Addon addon = addonService.getById(id);
            addon.setDownloads(addon.getDownloads() +1);
            addonService.update(addon);
            byte[] addonBinary = binaryService.getBinary(id);
            response.setContentType("application/octet-stream");
            String headerKey = "Content-Disposition";
            String headerValue = "attachment; filename=" + addonService.getById(id).getName() + ".rar";

            response.setHeader(headerKey, headerValue);
            ServletOutputStream outputStream = response.getOutputStream();

            outputStream.write(addonBinary);
            outputStream.close();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/{id}")
    public String showSingleAddon(@PathVariable int id, Model model, Principal principal) {

        try {
            Addon addon = addonService.getById(id);
            addAttributesToModel(model, principal);
            model.addAttribute("addons", addon);
            model.addAttribute("imgUtil", new ImageUtility());
            model.addAttribute("tags", addonService.getById(id).getTags());
            return "AddonView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @RequestMapping(value = "/{addon_id}", method = RequestMethod.POST)
    public String imageUpload(@PathVariable("addon_id") int addonId,
                              @RequestParam("file") MultipartFile file,
                              Model model) {

        try {
            addonImageService.update(file, addonId);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (BinaryFileException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/addons/{addon_id}";
        }
        return "redirect:/addons/{addon_id}";
    }

    @RequestMapping(value = "/{addon_id}/delete-image", method = RequestMethod.GET)
    public String imageDelete(@PathVariable("addon_id") int addonId, Model model) {

        try {
            Addon addon = addonService.getById(addonId);
            if (addon.getImage().getImageId() != 1) {
                AddonImage imageToDelete = addonImageService.getImageById(addon.getImage().getImageId());
                addon.setImage(addonImageService.getImageById(1));
                addonService.update(addon);
                addonImageService.delete(imageToDelete);
            }

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
        return "redirect:/addons/{addon_id}";
    }


    @GetMapping("/new")
    public String showNewAddonPage(Model model, Principal principal) {

        if (!isUserLoggedIn(principal)) {
            return "redirect:/login";
        }
        addAttributesToModel(model, principal);
        model.addAttribute("addonDto", new AddonDto());
        model.addAttribute("author", principal.getName());
        return "AddonNewView";
    }

    @PostMapping("/new")
    public String createAddon(@Valid @ModelAttribute("addonDto") AddonDto addonDto,
                              BindingResult bindingResult,
                              Model model,
                              Principal principal) {

        addAttributesToModel(model, principal);
        if (bindingResult.hasErrors()) {
            return "AddonNewView";
        }
        try {
            User author = userService.getByUsername(principal.getName());
            Addon addon = addonMapper.addonDtoToAddon(addonDto, author);
            addonService.create(addon, addonDto.getTag());
            int addon_id = addonService.getByName(addon.getName()).getAddonId();
            binaryService.uploadBinary(addonDto.getFile(), addon_id);
            return "redirect:/addons";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("name", "duplicate_name", e.getMessage());
            return "AddonNewView";
        } catch (BinaryFileException e) {
        bindingResult.rejectValue("file", "duplicate_name", e.getMessage());
        return "AddonNewView";
    }
    }

    @PostMapping("/{id}/update")
    public String updateAddon(@PathVariable int id,
                              @Valid @ModelAttribute("addonDto") AddonDto addonDto, BindingResult bindingResult,
                              Model model, Principal principal) {

        addAttributesToModel(model, principal);
        if (bindingResult.hasErrors()) {
            return "AddonUpdateView";
        }
        try {
            Addon addon = addonService.getById(id);
            addon.setName(addonDto.getName());
            addon.setDescription(addonDto.getDescription());
            addonService.update(addon);
            return "redirect:/addons/{id}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{id}/update")
    public String showUpdateAddonPage(@PathVariable int id, Model model, Principal principal) {

        if (!isUserLoggedIn(principal)) {
            return "redirect:/login";
        }

        String creatorUsername = addonService.getById(id).getCreator().getUsername();
        if (!principal.getName().equals(creatorUsername) && !userService.getByUsername(principal.getName()).isAdmin()) {
            return "AccessDeniedView";
        }
        try {
            Addon addon = addonService.getById(id);
            AddonDto addonDto = addonMapper.addonToAddonDto(addon);
            addAttributesToModel(model, principal);
            model.addAttribute("addonId", id);
            model.addAttribute("addonDto", addonDto);
            return "AddonUpdateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteAddon(@PathVariable int id, Model model, Principal principal) {

        if (!isUserLoggedIn(principal)) {
            return "redirect:/login";
        }

        String creatorUsername = addonService.getById(id).getCreator().getUsername();
        if (!principal.getName().equals(creatorUsername) && !userService.getByUsername(principal.getName()).isAdmin()) {
            return "AccessDeniedView";
        }

        try {
            addonService.delete(id);
            addAttributesToModel(model, principal);
            return "redirect:/addons";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{addonId}/rating/{rate}")
    public String addRating(
            @PathVariable int addonId, Model model, Principal principal, @PathVariable int rate) {

        try {
            authenticationHelper.tryGetUser(principal);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/login";
        }
        try {
            RatingDto ratingDto = new RatingDto(rate);
            Rating rating = ratingMapper.mapRatingDtoToRating(addonId, principal.getName(), ratingDto);
            addAttributesToModel(model, principal);
            ratingService.addRatingToAddon(rating);
            return "redirect:/addons/{addonId}";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (DuplicateEntityException e) {
            return "redirect:/addons/{addonId}";
        }
    }

    @GetMapping("/{id}/addTag")
    public String showAddTagPage(@PathVariable int id, Model model, Principal principal) {


        if (!isUserLoggedIn(principal)) {
            return "redirect:/login";
        }

        String creatorUsername = addonService.getById(id).getCreator().getUsername();
        if (!principal.getName().equals(creatorUsername) && !userService.getByUsername(principal.getName()).isAdmin()) {
            return "AccessDeniedView";
        }
        try {
            addAttributesToModel(model, principal);
            model.addAttribute("tagDto", new TagDto());
            model.addAttribute("addonName", addonService.getById(id).getName());
            return "AddonAddTagView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{id}/addTag")
    public String addTag(@PathVariable int id,
                         @Valid @ModelAttribute("tagDto") TagDto tagDto, BindingResult bindingResult,
                         Model model, Principal principal) {

        addAttributesToModel(model, principal);
        if (bindingResult.hasErrors()) {
            return "AddonAddTagView";
        }
        try {
            addonService.addTag(id, tagDto.getName());
            return "redirect:/addons/{id}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping()
    public String filter(@ModelAttribute AddonFilterDto addonFilterDto, Model model,
                         HttpSession session, Principal principal) {
        session.setAttribute("filter", addonFilterDto);
        model.addAttribute("imgUtil", new ImageUtility());
        model.addAttribute("addons", addonService.filter(addonFilterDto));
        model.addAttribute("addonFilterDto", new AddonFilterDto());
        addAttributesToModel(model, principal);
        return "AddonsView";
    }

    @PostMapping("/pending")
    public String filterNotApproved(@ModelAttribute AddonFilterDto addonFilterDto, Model model,
                                    HttpSession session, Principal principal) {
        session.setAttribute("filter", addonFilterDto);
        List<Addon> addons = addonService.filter(addonFilterDto);
        model.addAttribute("imgUtil", new ImageUtility());
        model.addAttribute("addons", addons);
        model.addAttribute("addons", addonService.filter(addonFilterDto));
        model.addAttribute("addonFilterDto", new AddonFilterDto());
        addAttributesToModel(model, principal);
        return "AddonsPendingView";
    }

}
