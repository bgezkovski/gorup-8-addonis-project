package com.example.addonis.controllers.mvc;

import com.example.addonis.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/profile")
public class ProfileMvcController {

    private final UserService service;

    @Autowired
    public ProfileMvcController(UserService service) {
        this.service = service;
    }

    @GetMapping
    public String getProfile(Principal principal) {
        int id = service.getByUsername(principal.getName()).getId();
        return "redirect:/users/" + id;
    }

}
