package com.example.addonis.controllers.mvc;

import com.example.addonis.exceptions.WrongActivationCodeException;
import com.example.addonis.models.dtos.CodeDto;
import com.example.addonis.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Controller
@RequestMapping("/verification")
public class VerifyUserMvcController extends BaseView {

    private final UserService service;

    @Autowired
    public VerifyUserMvcController(UserService service) {
        this.service = service;
    }

    @GetMapping("/{username}")
    public String sendEmailView(@PathVariable String username, Model model, Principal principal) {
        if (!isUserLoggedIn(principal) || !principal.getName().equals(username))
            return "AccessDeniedView";

        model.addAttribute("loggedIn", isUserLoggedIn(principal));
        model.addAttribute("code", new CodeDto());
        model.addAttribute("isSuccess", true);
        model.addAttribute("action", "/verification/" + principal.getName());

        return "SendVerificationMailPage";
    }

    @PostMapping("/{username}")
    public String validateUser(@ModelAttribute("code") String code, @PathVariable String username) {
        try {
            service.activateAccount(Integer.parseInt(code));
            int id = service.getByUsername(username).getId();
            return "redirect:/users/" + id;
        } catch (WrongActivationCodeException e) {
            return "redirect:/verification/" + username;
        }
    }
}
