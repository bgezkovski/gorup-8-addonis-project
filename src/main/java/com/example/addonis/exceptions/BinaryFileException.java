package com.example.addonis.exceptions;

public class BinaryFileException extends RuntimeException {
    public BinaryFileException(String message) {
        super(message);
    }
}
