package com.example.addonis.exceptions;

public class PhoneAlreadyExistsException extends DuplicateEntityException {
    public PhoneAlreadyExistsException(String type, String attribute, String value) {
        super(type, attribute, value);
    }
}