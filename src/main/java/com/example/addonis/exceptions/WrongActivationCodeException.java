package com.example.addonis.exceptions;

public class WrongActivationCodeException extends RuntimeException {
    public WrongActivationCodeException(String msg) {
        super(msg);
    }
}
