package com.example.addonis.exceptions;

public class GitHubServiceException extends RuntimeException {
    public GitHubServiceException(String message) {
        super(message);
    }
}
