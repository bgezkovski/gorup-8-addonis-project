package com.example.addonis.exceptions;

public class UsernameAlreadyExistsException extends DuplicateEntityException {
    public UsernameAlreadyExistsException(String type, String attribute, String value) {
        super(type, attribute, value);
    }
}
