package com.example.addonis.exceptions;

public class EmailAlreadyExistsException extends DuplicateEntityException {
    public EmailAlreadyExistsException(String type, String attribute, String value) {
        super(type, attribute, value);
    }
}
