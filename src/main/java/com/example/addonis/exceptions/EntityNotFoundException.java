package com.example.addonis.exceptions;


public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(String message) {
        super(message);
    }

    public EntityNotFoundException(String type, String attribute, String value) {
        super(String.format("%s with %s not found.", type, attribute, value));
    }

    public EntityNotFoundException(String type, String attribute, int value) {
        super(String.format("%s with %s not found.", type, attribute, value));
    }

    public EntityNotFoundException(String type, int id) {
        this(type, "id", String.valueOf(id));
    }

    public EntityNotFoundException(int voteId, int addonId, int userId) {
        super(String.format("Vote with Id: %d from Post with Id: %d created by User with Id: %d does not exist.",
                voteId, addonId, userId));
    }
}


