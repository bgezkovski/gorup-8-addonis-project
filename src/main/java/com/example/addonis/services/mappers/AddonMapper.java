package com.example.addonis.services.mappers;

import com.example.addonis.models.Addon;
import com.example.addonis.models.User;
import com.example.addonis.models.dtos.AddonDto;
import com.example.addonis.models.dtos.AddonDtoOut;
import com.example.addonis.services.contracts.AddonImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AddonMapper {

    private final UserMapper userMapper;

    private final AddonImageService addonImageService;

    @Autowired
    public AddonMapper(UserMapper userMapper, AddonImageService addonImageService) {
        this.userMapper = userMapper;
        this.addonImageService = addonImageService;
    }

    public Addon addonDtoToAddon(AddonDto addonDto, User user) {
        return new Addon(
                addonDto.getName(),
                addonDto.getTargetIDE(),
                user,
                addonDto.getDescription(),
                addonDto.getCreationTime(),
                addonDto.getDownloads(),
                addonDto.getSource_url(),
                addonImageService.getImageById(1),
                false);

    }

    public Addon addonDtoToAddon(AddonDto addonDto, User user, int id) {
        Addon addon = addonDtoToAddon(addonDto, user);
        addon.setAddonId(id);
        return addon;
    }

    public AddonDtoOut addonToAddonDtoOut(Addon addon) {
        return new AddonDtoOut(
                addon.getName(),
                addon.getTargetIDE(),
                addon.getDescription(),
                addon.getCreationTime(),
                addon.getDownloads(),
                userMapper.userToUserDtoOut(addon.getCreator()),
                addon.getTags()
        );
    }

    public AddonDto addonToAddonDto(Addon addon) {
        return new AddonDto(
                addon.getName(),
                addon.getTargetIDE(),
                addon.getDescription(),
                addon.getCreationTime(),
                addon.getDownloads(),
                addon.getTags(),
                addon.getSourceUrl()
        );
    }
}