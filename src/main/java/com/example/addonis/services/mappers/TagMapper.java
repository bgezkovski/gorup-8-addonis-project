package com.example.addonis.services.mappers;

import com.example.addonis.models.Tag;
import com.example.addonis.models.dtos.TagDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TagMapper {

    @Autowired
    public TagMapper() {
    }

    public Tag tagDtoToTag(TagDto tagDto) {
        return new Tag(
                tagDto.getName());
    }
}
