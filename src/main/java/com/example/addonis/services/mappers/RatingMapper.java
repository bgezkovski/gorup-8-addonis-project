package com.example.addonis.services.mappers;

import com.example.addonis.models.Addon;
import com.example.addonis.models.Rating;
import com.example.addonis.models.User;
import com.example.addonis.models.dtos.RatingDto;
import com.example.addonis.services.contracts.AddonService;
import com.example.addonis.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RatingMapper {

    private final AddonService addonService;
    private final UserService userService;

    @Autowired
    public RatingMapper(AddonService addonService, UserService userService) {
        this.addonService = addonService;
        this.userService = userService;
    }

    public Rating mapRatingDtoToRating(int addonId, String username, RatingDto ratingDto) {
        User user = userService.getByUsername(username);
        Addon addon = addonService.getById(addonId);

        Rating rating = new Rating();
        rating.setRating(ratingDto.getVote());
        rating.setUser(user);
        rating.setAddon(addon);

        return rating;
    }
}