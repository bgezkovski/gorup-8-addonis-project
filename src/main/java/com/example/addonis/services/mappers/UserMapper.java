package com.example.addonis.services.mappers;

import com.example.addonis.models.Image;
import com.example.addonis.models.Role;
import com.example.addonis.models.User;
import com.example.addonis.models.dtos.UserDto;
import com.example.addonis.models.dtos.UserDtoOut;
import com.example.addonis.models.dtos.UserFilterDto;
import com.example.addonis.services.contracts.ImageService;
import com.example.addonis.services.contracts.UserService;
import com.example.addonis.utils.ImageUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserMapper {
    private final UserService userService;

    private final ImageService imageService;

    @Autowired
    public UserMapper(UserService userService, ImageService imageService) {

        this.userService = userService;
        this.imageService = imageService;
    }

    public User userDtoToUser(UserDto userDto, int user_id) {
        User user = userDtoToUser(userDto);
        user.setId(user_id);
        Image image = userService.getById(user_id).getImage();
        user.setImage(image);
        user.setBlocked(userService.getById(user_id).isBlocked());
        user.setActivated(userService.getById(user_id).isActivated());

        return user;
    }

    public User userDtoToUser(UserDto userDto) {
        User user = new User();
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setPhone(userDto.getPhone());
        user.setImage(imageService.getImageById(1));
        Role role = userService.getRoleById(1);
        user.setRole(role);

        return user;
    }

    public UserDto userToUserDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        userDto.setUsername(user.getUsername());
        userDto.setPassword(user.getPassword());
        userDto.setPhone(user.getPhone());
        return userDto;
    }

    public UserDtoOut userToUserDtoOut(User user) {
        UserDtoOut userDtoOut = new UserDtoOut();
        userDtoOut.setFirstName(user.getFirstName());
        userDtoOut.setLastName(user.getLastName());
        userDtoOut.setUsername(user.getUsername());
        userDtoOut.setEmail(user.getEmail());
        userDtoOut.setPhone(user.getPhone());
        Image decompressedImage = new Image();
        decompressedImage.setImageId(user.getImage().getImageId());
        decompressedImage.setName(user.getImage().getName());
        decompressedImage.setType(user.getImage().getType());
        decompressedImage.setImage(ImageUtility.decompressImage(user.getImage().getImage()));
        userDtoOut.setImage(decompressedImage);

        return userDtoOut;
    }

    public UserFilterDto paramsToFilterDto(Optional<String> username,
                                           Optional<String> roleName,
                                           Optional<String> email,
                                           Optional<String> phone,
                                           Optional<String> sortBy,
                                           Optional<String> sortOrder) {

        UserFilterDto userFilterDto = new UserFilterDto();
        userFilterDto.setUsername(username);
        userFilterDto.setRoleName(roleName);
        userFilterDto.setEmail(email);
        userFilterDto.setPhone(phone);
        userFilterDto.setSortBy(sortBy);
        userFilterDto.setSortOrder(sortOrder);

        return userFilterDto;
    }
}