package com.example.addonis.services;

import com.example.addonis.exceptions.BinaryFileException;
import com.example.addonis.exceptions.EntityNotFoundException;
import com.example.addonis.models.AddonBinary;
import com.example.addonis.repositories.contracts.BinaryContentRepository;
import com.example.addonis.services.contracts.AddonBinaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class AddonBinaryServiceImpl implements AddonBinaryService {

    private final BinaryContentRepository repository;

    @Autowired
    public AddonBinaryServiceImpl(BinaryContentRepository repository) {
        this.repository = repository;
    }

    @Override
    public void uploadBinary(MultipartFile file, int id) {
        try {
            AddonBinary binary = new AddonBinary(id, file.getBytes());
            repository.saveBinaryContent(binary);
        } catch (IOException e) {
            throw new BinaryFileException("Error in binary upload");
        }
    }

    @Override
    public byte[] getBinary(int id) {
        try {
            return repository.getBinaryContent(id).getContent();
        } catch (EntityNotFoundException e) {
            throw new BinaryFileException(e.getMessage());
        }
    }
}
