package com.example.addonis.services;

import com.example.addonis.exceptions.ForbiddenOperationException;
import com.example.addonis.exceptions.WrongActivationCodeException;
import com.example.addonis.models.Image;
import com.example.addonis.models.Role;
import com.example.addonis.models.User;
import com.example.addonis.models.dtos.UserFilterDto;
import com.example.addonis.models.dtos.UserFilterDtoEmpty;
import com.example.addonis.repositories.contracts.ImageRepository;
import com.example.addonis.repositories.contracts.UserRepository;
import com.example.addonis.services.contracts.AddonService;
import com.example.addonis.services.contracts.EmailService;
import com.example.addonis.services.contracts.RatingService;
import com.example.addonis.services.contracts.UserService;
import com.example.addonis.utils.UserHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final ImageRepository imageRepository;
    private final AddonService addonService;
    private final RatingService ratingService;
    private final UserHelper userHelper;
    private final BCryptPasswordEncoder encoder;
    private final EmailService emailService;
    private final Map<Integer, String> usersToActivate;
    private final Map<Integer, Timestamp> codesValidity;
    private final Random random;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           UserHelper userHelper,
                           EmailService emailService,
                           ImageRepository imageRepository,
                           AddonService addonService,
                           RatingService ratingService) {

        this.userRepository = userRepository;
        this.userHelper = userHelper;
        this.emailService = emailService;
        this.imageRepository = imageRepository;
        this.addonService = addonService;
        this.ratingService = ratingService;
        encoder = new BCryptPasswordEncoder();
        usersToActivate = new HashMap<>();
        codesValidity = new HashMap<>();
        random = new Random();
    }

    @Override
    public List<User> getUsers(Optional<String> search) {
        return userRepository.getUsers(search);
    }

    @Override
    public List<User> getUsers(UserFilterDto userFilterDto) {
        return userRepository.get(userFilterDto);
    }

    @Override
    public List<User> getUsers(UserFilterDtoEmpty userFilterDtoEmpty) {
        UserFilterDto userFilterDto = new UserFilterDto();
        userFilterDto.setUsername(userFilterDtoEmpty.getUsername());
        userFilterDto.setEmail(userFilterDtoEmpty.getEmail());
        userFilterDto.setPhone(userFilterDtoEmpty.getPhone());
        userFilterDto.setRoleName(userFilterDtoEmpty.getRoleName());
        userFilterDto.setSortBy(userFilterDtoEmpty.getSortBy());
        userFilterDto.setSortOrder(userFilterDtoEmpty.getSortOrder());
        return userRepository.get(userFilterDto);
    }

    @Override
    public Page<User> findPaginated(Pageable pageable, List<User> users) {
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        int usersCount = users.size();
        List<User> page;

        if (usersCount < startItem) {
            page = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, usersCount);
            page = users.subList(startItem, toIndex);
        }

        Page<User> usersPage
                = new PageImpl<User>(page, PageRequest.of(currentPage, pageSize), usersCount);

        return usersPage;
    }

    @Override
    public User getById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByUsername(username);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByEmail(email);
    }

    @Override
    public User getByPhone(String phone) {
        return userRepository.getByPhone(phone);
    }

    @Override
    public int getCount() {
        return userRepository.getCount();
    }

    @Override
    public void create(User user) {
        userHelper.checkUsernameExists(user.getUsername());
        userHelper.checkEmailExists(user.getEmail());
        userHelper.checkPhoneExists(user.getPhone());
        user.setPassword(encoder.encode(user.getPassword()));
        user.setActivated(false);
        userRepository.create(user);
        sendActivationEmail(user);
    }

    @Override
    public void update(User user) {
        userHelper.checkUsernameExists(user.getUsername(), user.getId());
        userHelper.checkEmailExists(user.getEmail(), user.getId());
        userHelper.checkPhoneExists(user.getPhone(), user.getId());
        user.setPassword(encoder.encode(user.getPassword()));
        userRepository.update(user);
    }

    @Override
    public void delete(int user_id) {
        User userToDelete = getById(user_id);
        Image oldImage = userToDelete.getImage();
        userToDelete.setImage(imageRepository.getImageById(1));
        update(userToDelete);
        if (oldImage.getImageId() != 1) {
            imageRepository.delete(oldImage);
        }
        ratingService.getRatingsByUserId(user_id).
                forEach(ratingService::deleteRating);
        addonService.getAddonsByUserId(user_id).
                forEach(addon -> addonService.delete(addon.getAddonId()));
        userRepository.delete(user_id);
    }

    @Override
    public User promoteToAdmin(User userToPromote) {
        if (userToPromote.isAdmin()) {
            throw new UnsupportedOperationException("User is already an admin.");
        }
        userToPromote.setRole(userRepository.getRoleById(2));
        userRepository.update(userToPromote);
        return userToPromote;
    }

    @Override
    public User blockUser(User userToBlock) {
        if (userToBlock.isBlocked()) {
            throw new UnsupportedOperationException("User is already blocked.");
        }
        userToBlock.setBlocked(true);
        userRepository.update(userToBlock);
        return userToBlock;
    }

    @Override
    public User unblockUser(User userToUnblock) {
        if (!userToUnblock.isBlocked()) {
            throw new UnsupportedOperationException("User is already not blocked.");
        }
        userToUnblock.setBlocked(false);
        userRepository.update(userToUnblock);
        return userToUnblock;
    }

    @Override
    public Role getRoleById(int id) {
        return userRepository.getRoleById(id);
    }

    @Override
    public void activateAccount(int code) {
        if (!usersToActivate.containsKey(code))
            throw new WrongActivationCodeException("Code not active. Maybe user is activated already?");
        Timestamp now = Timestamp.from(Instant.now());
        if (codesValidity.get(code).before(now)) {
            usersToActivate.remove(code);
            codesValidity.remove(code);
            throw new WrongActivationCodeException("Code expired send new code");
        }
        User userToActivate = userRepository.getByUsername(usersToActivate.get(code));
        userToActivate.setActivated(true);
        usersToActivate.remove(code);
        codesValidity.remove(code);
        userRepository.update(userToActivate);
    }

    @Override
    public void resendActivationCode(String username) {

        User user = userRepository.getByUsername(username);
        if (user.isActivated())
            throw new ForbiddenOperationException("User already activated!");
        sendActivationEmail(user);

    }

    @Override
    public List<Role> getRoles() {
        return userRepository.getRoles();
    }

    @Override
    public void inviteFriend(String username, String email) {
        User user = getByUsername(username);
        userHelper.checkEmailExists(email);
        // Todo finish this with custom template and link
        emailService.sendMessage(email,
                String.format("%s %s sent you an initiation to join Addonis", user.getFirstName(), user.getLastName()),
                "Invitation Link");
    }

    @Override
    public List<String> getAllEmails() {
        return userRepository.getAllEmails();
    }

    @Override
    public List<String> getAllPhoneNumbers() {
        return userRepository.getAllPhoneNumbers();
    }

    @Override
    public List<String> getAllUsernames() {
        return userRepository.getAllUsernames();
    }

    private void sendActivationEmail(User user) {
        String email = user.getEmail();
        int code = getActivationCode(user);
        //emailService.sendMessage(email, "Account activation", String.valueOf(code));
        emailService.sendUserCreationVerificationCode(user, code);
    }

    private int getActivationCode(User user) {
        int code = getCode();
        if (usersToActivate.containsKey(code))
            getActivationCode(user);
        usersToActivate.put(code, user.getUsername());
        codesValidity.put(code, getActivationTime(5));
        return code;
    }

    private int getCode() {
        return 1000 + random.nextInt(2000);
    }

    private Timestamp getActivationTime(int minutes) {
        Timestamp out = Timestamp.from(Instant.now());
        out.setTime(out.getTime() + ((60 * minutes) * 1000));
        return out;
    }
}

