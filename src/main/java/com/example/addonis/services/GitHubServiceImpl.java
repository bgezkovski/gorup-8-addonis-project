package com.example.addonis.services;

import com.example.addonis.exceptions.GitHubServiceException;
import com.example.addonis.services.contracts.GitHubService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.sql.Date;
import java.text.ParseException;

@Service
public class GitHubServiceImpl implements GitHubService {

    private final RestTemplate restTemplate;
    private final HttpHeaders httpHeaders = new HttpHeaders();
    private final String masterUrl = "https://api.github.com/repos/";

    public GitHubServiceImpl(RestTemplateBuilder restTemplate) {
        this.restTemplate = restTemplate.build();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.set("Authorization", "token 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");
    }

    @Override
    public int getPullRequests(String owner, String repo) {
        String url = masterUrl + owner + "/" + repo + "/pulls";
        HttpEntity<String> entity = new HttpEntity<String>("body", httpHeaders);
        ResponseEntity<String> raw = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        try {
            return getPullRequestCountFromJSON(raw.getBody());
        } catch (JSONException e) {
            throw new GitHubServiceException("Error retrieving pull request from git hub");
        }
    }

    @Override
    public String getCommitMessage(String owner, String repo) {
        String url = masterUrl + owner + "/" + repo + "/commits";
        HttpEntity<String> entity = new HttpEntity<String>("body", httpHeaders);

        ResponseEntity<String> raw = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        try {
            return getCommitMessageFromJSON(raw.getBody()).strip();
        } catch (JSONException e) {
            throw new GitHubServiceException("Error retrieving commit message from git hub");
        }
    }

    @Override
    public Date getLastCommitDate(String owner, String repo) {
        String url = masterUrl + owner + "/" + repo + "/commits";
//        String raw = restTemplate.getForObject(url, String.class);
        HttpEntity<String> entity = new HttpEntity<String>("body", httpHeaders);

        ResponseEntity<String> raw = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        try {
            return getCommitTimestampFromJSON(raw.getBody());
        } catch (JSONException e) {
            throw new GitHubServiceException("Error retrieving commit date from git hub");
        } catch (ParseException e) {
            throw new GitHubServiceException("Error parsing commit date from git hub");
        }
    }

    @Override
    public int getIssues(String owner, String repo) {
        String url = masterUrl + owner + "/" + repo;
//        String raw = this.restTemplate.getForObject(url, String.class);
        HttpEntity<String> entity = new HttpEntity<String>("body", httpHeaders);

        ResponseEntity<String> raw = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

        try {
            return getIssuesFromJSON(raw.getBody());
        } catch (JSONException e) {
            throw new GitHubServiceException("Error retrieving issues count from git hub");
        }
    }

    private int getIssuesFromJSON(String rawJson) throws JSONException {
        JSONObject obj = new JSONObject(rawJson);
        return Integer.parseInt(obj.get("open_issues_count").toString());
    }

    private int getPullRequestCountFromJSON(String rawJson) throws JSONException {
        JSONArray arr = new JSONArray(rawJson);
        return arr.length();
    }

    private String getCommitMessageFromJSON(String rawJson) throws JSONException {
        JSONArray arr = new JSONArray(rawJson);
        JSONObject commit = (JSONObject) new JSONObject(arr.get(0).toString()).get("commit");
        String message = commit.get("message").toString().strip();
        //Check to make sure message is not longer than what the database field can hold
        if (message.length() > 512) {
            message = message.substring(0, 512 - 3).concat("...");
        }
        return message;
    }

    private Date getCommitTimestampFromJSON(String rawJson) throws JSONException, ParseException {
        JSONArray arr = new JSONArray(rawJson);
        JSONObject commit = (JSONObject) new JSONObject(arr.get(0).toString()).get("commit");
        String date = commit.getJSONObject("author").get("date").toString().split("T")[0];
        return Date.valueOf(date);
    }
}
