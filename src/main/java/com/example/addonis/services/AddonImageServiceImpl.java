package com.example.addonis.services;

import com.example.addonis.exceptions.BinaryFileException;
import com.example.addonis.models.Addon;
import com.example.addonis.models.AddonImage;
import com.example.addonis.repositories.contracts.AddonImageRepository;
import com.example.addonis.services.contracts.AddonImageService;
import com.example.addonis.services.contracts.AddonService;
import com.example.addonis.utils.ImageUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class AddonImageServiceImpl implements AddonImageService {

    private final AddonImageRepository addonImageRepository;

    private final AddonService addonService;

    @Autowired
    public AddonImageServiceImpl(AddonImageRepository addonImageRepository, AddonService addonService) {
        this.addonImageRepository = addonImageRepository;
        this.addonService = addonService;
    }

    @Override
    public AddonImage getImageById(int image_id) {
        return addonImageRepository.getImageById(image_id);
    }

    @Override
    public int create(MultipartFile file) {
        try {
            AddonImage image = new AddonImage(file.getOriginalFilename(),
                    file.getContentType(), ImageUtility.compressImage(file.getBytes()));

            return addonImageRepository.create(image);

        } catch (IOException e) {
            throw new BinaryFileException("Error in image upload");
        }
    }

    @Override
    public void update(MultipartFile file, int addonId) {
        if (file.isEmpty() || file.getSize() > 1000000) {
            throw new BinaryFileException("Addon image must not be empty and less than 1 МB in size.");
        }

        try {
            Addon addon = addonService.getById(addonId);
            AddonImage oldImage = addon.getImage();

            if (oldImage.getImageId() == 1) {

                int id = create(file);
                addon.setImage(getImageById(id));
                addonService.update(addon);
            } else {
                oldImage.setName(file.getOriginalFilename());
                oldImage.setType(file.getContentType());
                oldImage.setImage(ImageUtility.compressImage(file.getBytes()));
                addonImageRepository.update(oldImage);
            }
        } catch (IOException e) {
            throw new BinaryFileException("Error in image upload");
        }
    }

    @Override
    public void updateByImageId(MultipartFile file, int imageId) {
        if (file.getSize() > 1000000) {
            throw new BinaryFileException("Addon image must be less than 1 MB in size.");
        }
        try {
            AddonImage oldImage = addonImageRepository.getImageById(imageId);
            oldImage.setName(file.getOriginalFilename());
            oldImage.setType(file.getContentType());
            oldImage.setImage(ImageUtility.compressImage(file.getBytes()));
            addonImageRepository.update(oldImage);
        } catch (IOException e) {
            throw new BinaryFileException("Error in image upload");
        }
    }

    @Override
    public void delete(AddonImage image) {
        addonImageRepository.delete(image);
    }
}
