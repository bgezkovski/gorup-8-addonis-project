package com.example.addonis.services;

import com.example.addonis.exceptions.BinaryFileException;
import com.example.addonis.models.Image;
import com.example.addonis.models.User;
import com.example.addonis.repositories.contracts.ImageRepository;
import com.example.addonis.services.contracts.ImageService;
import com.example.addonis.services.contracts.UserService;
import com.example.addonis.utils.ImageUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class ImageServiceImpl implements ImageService {

    private final ImageRepository imageRepository;

    private final UserService userService;

    @Autowired
    public ImageServiceImpl(ImageRepository imageRepository, UserService userService) {
        this.imageRepository = imageRepository;
        this.userService = userService;
    }

    @Override
    public Image getImageById(int image_id) {
        return imageRepository.getImageById(image_id);
    }

    @Override
    public int create(MultipartFile file) {
        try {
            Image image = new Image(file.getOriginalFilename(),
                    file.getContentType(), ImageUtility.compressImage(file.getBytes()));

            return imageRepository.create(image);

        } catch (IOException e) {
            throw new BinaryFileException("Error in image upload");
        }
    }

    @Override
    public void update(MultipartFile file, int userId) {
        if (file.isEmpty() || file.getSize() > 1000000) {
            throw new BinaryFileException("Profile image must not be empty and less than 1 МB in size.");
        }

        try {
            User user = userService.getById(userId);
            Image oldImage = user.getImage();

            if (oldImage.getImageId() == 1) {

                int id = create(file);
                user.setImage(getImageById(id));
                userService.update(user);
            } else {
                oldImage.setName(file.getOriginalFilename());
                oldImage.setType(file.getContentType());
                oldImage.setImage(ImageUtility.compressImage(file.getBytes()));
                imageRepository.update(oldImage);
            }
        } catch (IOException e) {
            throw new BinaryFileException("Error in image upload");
        }
    }

    @Override
    public void updateByImageId(MultipartFile file, int imageId) {
        if (file.getSize() > 1000000) {
            throw new BinaryFileException("Profile image must be less than 1 MB in size.");
        }
        try {
            Image oldImage = imageRepository.getImageById(imageId);
            oldImage.setName(file.getOriginalFilename());
            oldImage.setType(file.getContentType());
            oldImage.setImage(ImageUtility.compressImage(file.getBytes()));
            imageRepository.update(oldImage);
        } catch (IOException e) {
            throw new BinaryFileException("Error in image upload");
        }

    }

    @Override
    public void delete(Image image) {
        imageRepository.delete(image);
    }
}
