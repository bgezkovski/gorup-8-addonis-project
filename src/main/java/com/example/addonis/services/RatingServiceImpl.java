package com.example.addonis.services;

import com.example.addonis.models.Rating;
import com.example.addonis.repositories.contracts.RatingRepository;
import com.example.addonis.services.contracts.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class RatingServiceImpl implements RatingService {

    private final RatingRepository repository;

    @Autowired
    public RatingServiceImpl(RatingRepository repository) {
        this.repository = repository;
    }

    @Override
    public void addRatingToAddon(Rating rating) {
        if (repository.ratingDoesNotExists(rating.getUser().getId(), rating.getAddon().getAddonId())) {
            repository.create(rating);
        }
    }

    @Override
    public void deleteRating(Rating rating) {
        repository.delete(rating);

    }

    @Override
    public double getAverageRating(int id) {
        if (repository.getMappedRating().containsKey(id)) {
            return repository.getMappedRating().get(id);
        } else {
            return 0;
        }
    }

    @Override
    public Map<Integer, Double> getMappedRating() {
        return repository.getMappedRating();
    }


    @Override
    public List<Rating> getRatingsByAddonId(int id) {
        return repository.getRatingsByAddonId(id);
    }

    @Override
    public List<Rating> getRatingsByUserId(int user_id) {
        return repository.getRatingsByUserId(user_id);
    }

}
