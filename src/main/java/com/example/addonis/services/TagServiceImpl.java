package com.example.addonis.services;

import com.example.addonis.exceptions.DuplicateEntityException;
import com.example.addonis.exceptions.EntityNotFoundException;
import com.example.addonis.models.Tag;
import com.example.addonis.repositories.contracts.TagRepository;
import com.example.addonis.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagServiceImpl implements TagService {

    TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public List<Tag> getTags() {
        return tagRepository.getTags();
    }

    @Override
    public Tag getById(int id) {
        return tagRepository.getById(id);
    }

    @Override
    public void create(Tag tag) {
        boolean tagNameExists = true;

        try {
            tagRepository.getByName(tag.getName());
        } catch (EntityNotFoundException e) {
            tagNameExists = false;
        }

        if (tagNameExists) {
            throw new DuplicateEntityException("Tag", "name", tag.getName());
        }

        tagRepository.create(tag);
    }

    @Override
    public void delete(int id) {
        tagRepository.delete(id);
    }
}
