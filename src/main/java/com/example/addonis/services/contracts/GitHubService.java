package com.example.addonis.services.contracts;

import java.sql.Date;

public interface GitHubService {

    int getPullRequests(String owner, String repo);

    String getCommitMessage(String owner, String repo);

    Date getLastCommitDate(String owner, String repo);

    int getIssues(String owner, String repo);
}
