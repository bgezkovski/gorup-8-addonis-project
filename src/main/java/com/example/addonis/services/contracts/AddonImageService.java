package com.example.addonis.services.contracts;

import com.example.addonis.models.AddonImage;
import org.springframework.web.multipart.MultipartFile;

public interface AddonImageService {

    AddonImage getImageById(int imageId);

    int create(MultipartFile file);

    void update(MultipartFile file, int userId);

    void updateByImageId(MultipartFile file, int imageId);

    void delete(AddonImage image);
}
