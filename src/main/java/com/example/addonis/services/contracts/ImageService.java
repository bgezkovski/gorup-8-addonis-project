package com.example.addonis.services.contracts;

import com.example.addonis.models.Image;
import org.springframework.web.multipart.MultipartFile;

public interface ImageService {
    Image getImageById(int imageId);

    int create(MultipartFile file);

    void update(MultipartFile file, int userId);

    void updateByImageId(MultipartFile file, int imageId);

    void delete(Image image);
}
