package com.example.addonis.services.contracts;

import com.example.addonis.exceptions.DuplicateEntityException;
import com.example.addonis.exceptions.EntityNotFoundException;
import com.example.addonis.models.Addon;
import com.example.addonis.models.dtos.AddonFilterDto;

import java.util.List;
import java.util.Optional;

public interface AddonService {

    /**
     * Gets a tag by name and adds it to an addon with given id.
     *
     * @param addonId
     * @param tagName
     * @throws DuplicateEntityException
     */
    void addTag(int addonId, String tagName);

    /**
     * Removes a tag from a given addon id.
     *
     * @param addonId
     * @param tagName
     * @throws EntityNotFoundException
     */
    void removeTag(int addonId, String tagName);

    void approveAddon(int addonId);

    void featureAddon(int addonId);

    void removeFeatureAddon(int addonId);

    /**
     * Gets a certain addon by id and returns that addon.
     *
     * @param id addon's id
     * @return addon object
     * @throws DuplicateEntityException
     */
    Addon getById(int id);

    /**
     * Creates an addon through a sessionFactory
     *
     * @param addon object
     * @param tag
     * @return
     */
    int create(Addon addon, String tag);

    /**
     * Updates an addon through a sessionFactory
     *
     * @param addon object
     */
    void update(Addon addon);

    /**
     * Delete the addon with the given id
     *
     * @param id addon's id
     */
    void delete(int id);

    /**
     * Creates a List of all the addons in the database
     *
     * @return query list of all addons
     */

    void getGitData(Addon addon);

    List<Addon> getAddons(Optional<String> search);


    List<Addon> getAddonsByUserId(int user_id);

    /**
     * Creates new object from FilterOptions and fills that object from addonFilterDto
     * works with SortBy and SortOrder parameters
     *
     * @param addonFilterDto
     * @return list of filtered addons by the given parameters
     */
    List<Addon> filter(AddonFilterDto addonFilterDto);

    List<Addon> getMostRecent();

    List<Addon> getMostDownloaded();

    List<Addon> getFeatured();

    Addon getByName(String name);

    void getGitDataForAddons();
}