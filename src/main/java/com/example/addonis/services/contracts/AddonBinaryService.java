package com.example.addonis.services.contracts;


import org.springframework.web.multipart.MultipartFile;

public interface AddonBinaryService {

    /**
     * Upload a binary file to the database
     *
     * @param file     MultipartFile
     * @param addon_id id of addon to associate this binary with
     */
    void uploadBinary(MultipartFile file, int addon_id);

    /**
     * Get bytes of desired binary using addon id
     *
     * @param id id of addon to get binary from
     * @return byte array
     */
    byte[] getBinary(int id);
}
