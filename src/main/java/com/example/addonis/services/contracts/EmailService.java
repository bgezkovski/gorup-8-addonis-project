package com.example.addonis.services.contracts;

import com.example.addonis.models.User;

public interface EmailService {

    void sendMessage(String to, String subject, String text);

    void sendUserCreationVerificationCode(User user, int code);

    void sendAddonCreationVerificationCode(User user, int code);
}
