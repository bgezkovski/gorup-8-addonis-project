package com.example.addonis.services.contracts;

import com.example.addonis.models.Rating;

import java.util.List;
import java.util.Map;

public interface RatingService {

    void addRatingToAddon(Rating rating);

    void deleteRating(Rating rating);

    double getAverageRating(int id);

    Map<Integer, Double> getMappedRating();

    List<Rating> getRatingsByAddonId(int id);


    List<Rating> getRatingsByUserId(int user_id);
}