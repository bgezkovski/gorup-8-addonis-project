package com.example.addonis.services.contracts;

import com.example.addonis.exceptions.EntityNotFoundException;
import com.example.addonis.models.Tag;

import java.util.List;

public interface TagService {

    /**
     * Creates a List of all the tags in the database
     *
     * @return query list of all tags
     */
    List<Tag> getTags();

    /**
     * Gets a certain tag by id and returns that tag.
     *
     * @param id tag's id
     * @return tag object
     * @throws EntityNotFoundException
     */
    Tag getById(int id);

    /**
     * Creates a tag through a sessionFactory
     *
     * @param tag object
     */
    void create(Tag tag);

    /**
     * Delete the tag with the given id
     *
     * @param id
     */
    void delete(int id);
}
