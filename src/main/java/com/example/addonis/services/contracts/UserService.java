package com.example.addonis.services.contracts;

import com.example.addonis.models.Role;
import com.example.addonis.models.User;
import com.example.addonis.models.dtos.UserFilterDto;
import com.example.addonis.models.dtos.UserFilterDtoEmpty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getUsers(Optional<String> search);

    List<User> getUsers(UserFilterDto userFilterDto);

    List<User> getUsers(UserFilterDtoEmpty userFilterDtoEmpty);

    Page<User> findPaginated(Pageable pageable, List<User> users);

    User getById(int id);

    User getByUsername(String username);

    User getByEmail(String email);

    User getByPhone(String phone);

    int getCount();

    void create(User user);

    void update(User user);

    void delete(int id);

    User promoteToAdmin(User userToPromote);

    User blockUser(User userToBlock);

    User unblockUser(User userToUnblock);

    Role getRoleById(int i);

    void activateAccount(int code);

    void resendActivationCode(String username);

    List<Role> getRoles();

    void inviteFriend(String user, String email);

    List<String> getAllEmails();

    List<String> getAllPhoneNumbers();

    List<String> getAllUsernames();

}

