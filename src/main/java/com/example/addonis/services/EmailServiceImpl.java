package com.example.addonis.services;

import com.example.addonis.models.User;
import com.example.addonis.services.contracts.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import static java.lang.String.format;

@Component
public class EmailServiceImpl implements EmailService {

    private JavaMailSender mailSender;

    @Autowired
    public EmailServiceImpl(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    private String userCreationTemplate(int code, User user) {
        return format("Hello %s %s, \n welcome to Addonis! \n To activate your account use this code\n %d",
                user.getFirstName(), user.getLastName(), code);
    }

    private String addonCreationTemplate(int code, User user) {
        return (format("Hello %s %s, \n To authorize the creation of your addon use this code\n %d",
                user.getFirstName(), user.getLastName(), code));
    }

    @Async
    public void sendMessage(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("info.addonis@gmail.com");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        mailSender.send(message);
    }

    @Override
    public void sendUserCreationVerificationCode(User user, int code) {
        String msg = userCreationTemplate(code, user);
        sendMessage(user.getEmail(), "Addonis account created", msg);
    }

    @Override
    public void sendAddonCreationVerificationCode(User user, int code) {
        String msg = addonCreationTemplate(code, user);
        sendMessage(user.getEmail(), "Add-on creation authorization code", msg);
    }
}
