package com.example.addonis.services;

import com.example.addonis.exceptions.DuplicateEntityException;
import com.example.addonis.exceptions.EntityNotFoundException;
import com.example.addonis.exceptions.GitHubServiceException;
import com.example.addonis.models.Addon;
import com.example.addonis.models.Tag;
import com.example.addonis.models.User;
import com.example.addonis.models.dtos.AddonFilterDto;
import com.example.addonis.repositories.contracts.AddonRepository;
import com.example.addonis.repositories.contracts.BinaryContentRepository;
import com.example.addonis.repositories.contracts.TagRepository;
import com.example.addonis.services.contracts.AddonService;
import com.example.addonis.services.contracts.EmailService;
import com.example.addonis.services.contracts.GitHubService;
import com.example.addonis.services.contracts.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;

@Service
public class AddonServiceImpl implements AddonService {

    private final AddonRepository repository;

    private final TagRepository tagRepository;

    private final BinaryContentRepository binaryRepository;

    private final GitHubService gitHubService;

    private final EmailService emailService;

    private final RatingService ratingService;

    private Map<Integer, String> usersToActivate;

    private Map<Integer, Timestamp> codesValidity;

    private Random random;

    @Autowired
    public AddonServiceImpl(AddonRepository addonRepository,
                            TagRepository tagRepository,
                            BinaryContentRepository binaryRepository,
                            GitHubService gitHubService,
                            EmailService emailService,
                            RatingService ratingService) {
        this.repository = addonRepository;
        this.tagRepository = tagRepository;
        this.binaryRepository = binaryRepository;
        this.gitHubService = gitHubService;
        this.emailService = emailService;
        this.ratingService = ratingService;
        this.usersToActivate = new HashMap<>();
        this.codesValidity = new HashMap<>();
        this.random = new Random();
    }

    @Override
    public void addTag(int addonId, String tagName) {
        Addon addon = repository.getById(addonId);
        Tag tagToAdd;
        try {
            tagToAdd = tagRepository.getByName(tagName);
        } catch (EntityNotFoundException e) {
            tagRepository.create(new Tag(tagName));
            tagToAdd = tagRepository.getByName(tagName);
        }
        if (addon.getTags().stream().anyMatch(b -> b.getName().equals(tagName))) {
            throw new DuplicateEntityException("Tag", "tagName", tagName);
        }
        addon.getTags().add(tagToAdd);
        repository.update(addon);
    }

    @Override
    public void removeTag(int addonId, String tagName) {
        Addon addon = repository.getById(addonId);
        Tag tagToRemove = tagRepository.getByName(tagName);
        if (tagToRemove == null) {
            throw new EntityNotFoundException("Tag", "tagName", tagName);
        }
        addon.getTags().remove(tagToRemove);
        repository.update(addon);
    }

    @Override
    public void approveAddon(int addonId) {
        Addon addon = repository.getById(addonId);
        addon.setApproved(true);
        repository.update(addon);
    }

    @Override
    public void featureAddon(int addonId) {
        Addon addon = repository.getById(addonId);
        addon.setFeatured(true);
        repository.update(addon);
    }

    @Override
    public void removeFeatureAddon(int addonId) {
        Addon addon = repository.getById(addonId);
        addon.setFeatured(false);
        repository.update(addon);
    }


    @Override
    public Addon getById(int id) {
        try {
            Addon addon = repository.getById(id);
            addon.setAverageRating(Math.round(ratingService.getAverageRating(id)));
            return addon;
        } catch (NoResultException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public int create(Addon addon, String tag) {
        boolean addonExists = true;
        try {
            repository.getByName(addon.getName());
        } catch (EntityNotFoundException e) {
            addonExists = false;
        }
        if (addonExists) {
            throw new DuplicateEntityException("Addon", "name", addon.getName());
        }
        int addon_id = repository.create(addon);
        getGitData(addon);
        addTag(addon_id, tag);
        return addon_id;
    }

    @Override
    public void update(Addon addon) {
        repository.update(addon);
    }

    @Override
    public void delete(int id) {
        tagRepository.deleteTagsByAddonId(id);
        binaryRepository.deleteBinaryByAddonId(id);
        ratingService.getRatingsByAddonId(id).stream().forEach(rating -> {
            ratingService.deleteRating(rating);
        });
        repository.delete(id);
    }

    @Override
    @Async
    public void getGitData(Addon addon) {
        String[] repoUrl = addon.getSourceUrl().split("/");
        String owner = repoUrl[repoUrl.length - 2];
        String repo = repoUrl[repoUrl.length - 1];
        addon.setPullRequests(gitHubService.getPullRequests(owner, repo));
        addon.setOpenIssues(gitHubService.getIssues(owner, repo));
        addon.setLastCommitDate(gitHubService.getLastCommitDate(owner, repo));
        addon.setLastCommitMessage(gitHubService.getCommitMessage(owner, repo));
        repository.update(addon);
    }

    @Override
    public List<Addon> getAddons(Optional<String> search) {
        List<Addon> addons = repository.getAddons();

        addons.forEach(addon -> addon.setAverageRating(
                Math.round(ratingService.getAverageRating(addon.getAddonId()))));
        return addons;
    }

    @Override
    public List<Addon> getAddonsByUserId(int user_id) {
        return repository.getAddonsByUserId(user_id);
    }

    @Override
    public List<Addon> filter(AddonFilterDto addonFilterDto) {
        return repository.filter(addonFilterDto);
    }

    @Override
    public List<Addon> getMostRecent() {
        return repository.getMostRecent();
    }

    @Override
    public List<Addon> getMostDownloaded() {
        return repository.getMostDownloaded();
    }

    @Override
    public List<Addon> getFeatured() {
        return repository.getFeatured();
    }

    @Override
    public Addon getByName(String name) {
        return repository.getByName(name);
    }

    private void sendActivationEmail(User user) {
        int code = getActivationCode(user);
        emailService.sendAddonCreationVerificationCode(user, code);
        System.out.println(code);
    }

    private int getActivationCode(User user) {
        int code = getCode();
        if (usersToActivate.containsKey(code))
            getActivationCode(user);
        usersToActivate.put(code, user.getUsername());
        codesValidity.put(code, getActivationTime(5));
        return code;
    }

    private int getCode() {
        return 1000 + random.nextInt(2000);
    }

    private Timestamp getActivationTime(int minutes) {
        Timestamp out = Timestamp.from(Instant.now());
        out.setTime(out.getTime() + ((60 * minutes) * 1000));
        return out;
    }

    @Async
    @Scheduled(initialDelay = 1000L, fixedRate = (1000L * 60L * 60L))
    @Override
    public void getGitDataForAddons() {
        List<Addon> addons = getAddons(Optional.empty());
        for (Addon addon : addons) {
            try {
                getGitData(addon);
            } catch (IndexOutOfBoundsException | GitHubServiceException ignored) {
            }
        }
    }
}