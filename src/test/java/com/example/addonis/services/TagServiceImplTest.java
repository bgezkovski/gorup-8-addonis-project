package com.example.addonis.services;

import com.example.addonis.exceptions.DuplicateEntityException;
import com.example.addonis.exceptions.EntityNotFoundException;
import com.example.addonis.models.Addon;
import com.example.addonis.models.Tag;
import com.example.addonis.repositories.contracts.AddonRepository;
import com.example.addonis.repositories.contracts.TagRepository;
import com.example.addonis.services.contracts.GitHubService;
import com.example.addonis.services.contracts.TagService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import static com.example.addonis.utils.MockHelpers.createMockAddon;
import static com.example.addonis.utils.MockHelpers.createMockTag;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class TagServiceImplTest {

    @Mock
    TagRepository mockTagRepository;

    @Mock
    AddonRepository mockAddonRepository;

    @Mock
    GitHubService mockGitHubService;

    @InjectMocks
    AddonServiceImpl mockAddonService;

    @InjectMocks
    TagServiceImpl mockTagService;

    @Test
    public void create_Should_CallRepository_When_TagWithSameNameDoesntExist() {
        Tag mockTag = createMockTag();
        Mockito.when(mockTagRepository.getByName("TestTag")).thenThrow(EntityNotFoundException.class);
        mockTagService.create(mockTag);

        verify(mockTagRepository, times(1))
                .create(mockTag);
    }

    @Test
    public void create_Should_ThrowException_When_TagNameExists() {
        Tag mockTag = createMockTag();
        TagService tagService = new TagServiceImpl(mockTagRepository);
        Mockito.when(mockTagRepository.getByName("TestTag")).thenReturn(mockTag);

        Assertions.assertThrows(DuplicateEntityException.class, () -> tagService.create(mockTag));
    }


    @Test
    public void addTag_should_throwException_when_tagDoesNotExists() {
        Addon mockAddon = createMockAddon();
        Mockito.when(mockAddonRepository.getById(Mockito.anyInt())).thenReturn(mockAddon);
        Mockito.when(mockTagRepository.getByName("TestTag")).thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockAddonService.addTag(2, "TestTag"));

    }

    @Test
    public void addTag_should_callAddonRepository_when_tagInAddon_doesNotExists() {
        Addon mockAddon = createMockAddon();
        Tag mockTag = createMockTag();
        mockTag.setName("NewTestTag");
        Mockito.when(mockAddonRepository.getById(Mockito.anyInt())).thenReturn(mockAddon);
        Mockito.when(mockTagRepository.getByName("NewTestTag")).thenReturn(mockTag);
        mockAddonService.addTag(1, "NewTestTag");

        Mockito.verify(mockAddonRepository, Mockito.times(1)).update(mockAddon);
    }


    @Test
    public void addTag_should_throwException_when_tagExistsInAddon() {
        Addon mockAddon = createMockAddon();
        Tag mockTag = createMockTag();
        Mockito.when(mockAddonRepository.getById(Mockito.anyInt())).thenReturn(mockAddon);
        Mockito.when(mockTagRepository.getByName("TestTag")).thenReturn(mockTag);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockAddonService.addTag(2, "TestTag"));
    }

    @Test
    public void removeTag_should_throwException_when_tagDoesNotExist() {
        Addon mockAddon = createMockAddon();
        Tag mockTag = createMockTag();
        Mockito.when(mockAddonRepository.getById(mockAddon.getAddonId())).thenReturn(mockAddon);
        Mockito.when(mockTagRepository.getByName(mockTag.getName())).thenReturn(null);

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockAddonService.removeTag(mockAddon.getAddonId(), mockTag.getName()));
    }


    @Test
    public void removeTag_should_call_addonRepository_when_tagExists() {
        Addon mockAddon = createMockAddon();
        Tag mockTag = createMockTag();
        Mockito.when(mockAddonRepository.getById(mockAddon.getAddonId())).thenReturn(mockAddon);
        Mockito.when(mockTagRepository.getByName(mockTag.getName())).thenReturn(mockTag);
        mockAddonService.removeTag(mockAddon.getAddonId(), mockTag.getName());

        Mockito.verify(mockAddonRepository, Mockito.times(1)).update(mockAddon);
    }
}
