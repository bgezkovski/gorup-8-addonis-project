package com.example.addonis.services;

import com.example.addonis.exceptions.DuplicateEntityException;
import com.example.addonis.exceptions.EntityNotFoundException;
import com.example.addonis.models.Role;
import com.example.addonis.models.User;
import com.example.addonis.repositories.contracts.UserRepository;
import com.example.addonis.services.contracts.AddonService;
import com.example.addonis.services.contracts.EmailService;
import com.example.addonis.services.contracts.ImageService;
import com.example.addonis.services.contracts.RatingService;
import com.example.addonis.utils.UserHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.addonis.utils.MockHelpers.createMockUser;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    @Mock
    UserRepository mockRepository;

    @Mock
    UserHelper mockUserHelper;

    @Mock
    EmailService emailService;

    @InjectMocks
    UserServiceImpl userService;

    @Test
    public void create_Should_CallRepository_When_UsernameAndEmailAndPhoneAreUnique(){
        Mockito.doNothing().when(mockUserHelper).checkUsernameExists("testUsername");
        Mockito.doNothing().when(mockUserHelper).checkEmailExists("testEmail");
        Mockito.doNothing().when(mockUserHelper).checkPhoneExists("testPhone");
        User user = createMockUser();
        userService.create(user);

        Mockito.verify(mockRepository, times(1)).create(user);
    }

    @Test
    public void create_Should_ThrowException_When_UsernameNotUnique(){
        Mockito.doThrow(DuplicateEntityException.class).when(mockUserHelper).checkUsernameExists("testUsername");
        User user = createMockUser();

        Assertions.assertThrows(DuplicateEntityException.class, ()->userService.create(user));
    }

    @Test
    public void create_Should_ThrowException_When_EmailNotUnique(){
        Mockito.doThrow(DuplicateEntityException.class).when(mockUserHelper).checkEmailExists("testEmail");
        Mockito.doNothing().when(mockUserHelper).checkUsernameExists("testUsername");
        User user = createMockUser();

        Assertions.assertThrows(DuplicateEntityException.class, ()->userService.create(user));
    }

    @Test
    public void create_Should_ThrowException_When_PhoneNotUnique(){
        Mockito.doThrow(DuplicateEntityException.class).when(mockUserHelper).checkPhoneExists("testPhone");
        Mockito.doNothing().when(mockUserHelper).checkUsernameExists("testUsername");
        Mockito.doNothing().when(mockUserHelper).checkEmailExists("testEmail");
        User user = createMockUser();

        Assertions.assertThrows(DuplicateEntityException.class, ()->userService.create(user));
    }

    @Test
    public  void update_Should_ThrowException_When_UsernameNotUnique(){
        Mockito.doThrow(DuplicateEntityException.class).when(mockUserHelper).checkUsernameExists("testUsername", 1);
        User user = createMockUser();

        Assertions.assertThrows(DuplicateEntityException.class, ()->userService.update(user));
    }

    @Test
    public  void update_Should_ThrowException_When_EmailNotUnique(){
        Mockito.doThrow(DuplicateEntityException.class).when(mockUserHelper).checkEmailExists("testEmail", 1);
        Mockito.doNothing().when(mockUserHelper).checkUsernameExists("testUsername", 1);
        User user = createMockUser();

        Assertions.assertThrows(DuplicateEntityException.class, ()->userService.update(user));
    }

    @Test
    public  void update_Should_ThrowException_When_PhoneNotUnique(){
        Mockito.doThrow(DuplicateEntityException.class).when(mockUserHelper).checkPhoneExists("testPhone", 1);
        Mockito.doNothing().when(mockUserHelper).checkUsernameExists("testUsername", 1);
        Mockito.doNothing().when(mockUserHelper).checkEmailExists("testEmail", 1);
        User user = createMockUser();

        Assertions.assertThrows(DuplicateEntityException.class, ()->userService.update(user));
    }

    @Test
    public  void update_Should_CallRepository_When_UsernameAndEmailAndPhoneAreUnique(){
        Mockito.doNothing().when(mockUserHelper).checkUsernameExists("testUsername", 1);
        Mockito.doNothing().when(mockUserHelper).checkEmailExists("testEmail", 1);
        Mockito.doNothing().when(mockUserHelper).checkPhoneExists("testPhone", 1);
        User user = createMockUser();
        userService.update(user);

        Mockito.verify(mockRepository, times(1)).update(user);
    }


    @Test
    public  void promote_Should_CallRepository_When_UserNotAdmin(){
        User userToPromote = createMockUser();
        userToPromote.setRole(new Role("user"));
        userService.update(userToPromote);

        Mockito.verify(mockRepository, times(1)).update(userToPromote);
    }

    @Test
    public  void block_Should_ThrowException_When_UserAlreadyBlocked(){
        User userToBlock = createMockUser();
        userToBlock.setBlocked(true);

        Assertions.assertThrows(UnsupportedOperationException.class, ()->userService.blockUser(userToBlock));
    }

    @Test
    public  void block_Should_CallRepository_When_UserNotBlocked(){
        User userToBlock = new User();
        Role role = new Role();
        role.setId(1);
        userToBlock.setRole(role);
        userService.blockUser(userToBlock);

        Mockito.verify(mockRepository, times(1)).update(userToBlock);
    }

    @Test
    public  void unblock_Should_ThrowException_When_UserAlreadyNotBlocked(){
        User userToUnblock = new User();
        Role role = new Role();
        role.setId(1);
        userToUnblock.setRole(role);

        Assertions.assertThrows(UnsupportedOperationException.class, ()->userService.unblockUser(userToUnblock));
    }

    @Test
    public  void unblock_Should_CallRepository_When_UserBlocked(){
        User userToUnblock = createMockUser();
        userToUnblock.setBlocked(true);
        userService.unblockUser(userToUnblock);

        Mockito.verify(mockRepository, times(1)).update(userToUnblock);
    }
}
