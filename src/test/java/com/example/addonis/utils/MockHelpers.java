package com.example.addonis.utils;

import com.example.addonis.models.Addon;
import com.example.addonis.models.Role;
import com.example.addonis.models.Tag;
import com.example.addonis.models.User;
import java.util.HashSet;
import java.util.Set;

public class MockHelpers {

    public static User createMockUser(){
        User mockUser = new User();
        Role mockRole = new Role("user");
        mockUser.setId(1);
        mockUser.setFirstName("testFirstName");
        mockUser.setLastName("testLastName");
        mockUser.setUsername("testUsername");
        mockUser.setPassword("testPassword");
        mockUser.setPhone("testPhone");
        mockUser.setEmail("testEmail");
        mockUser.setRole(mockRole);
        return mockUser;
    }

    public static Addon createMockAddon (){
        Addon mockAddon = new Addon();
        User user = new User();
        mockAddon.setAddonId(1);
        mockAddon.setName("TestName");
        mockAddon.setDescription("TestDescription");
        mockAddon.setTargetIDE("TestIDE");
        mockAddon.setCreator(user);
        mockAddon.setTags(getTagSet());
        return mockAddon;
    }

    public static Set<Tag> getTagSet() {
        Set<Tag> tags = new HashSet<>();
        tags.add(createMockTag());
        return tags;
    }

    public static Tag createMockTag() {
        var mockTag = new Tag();
        mockTag.setId(1);
        mockTag.setName("TestTag");
        return mockTag;
    }
}
